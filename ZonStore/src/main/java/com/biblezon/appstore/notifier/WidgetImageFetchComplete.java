package com.biblezon.appstore.notifier;

import android.graphics.Bitmap;

import java.util.List;

/**
 * Created by tunde00 on 12/13/2015.
 */
public interface WidgetImageFetchComplete {

    void onWidgetImageCompleted(List<Bitmap> bitmaps);

}
