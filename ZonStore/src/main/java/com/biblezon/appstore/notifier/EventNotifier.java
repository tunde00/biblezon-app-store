package com.biblezon.appstore.notifier;

public class EventNotifier {

	 private EventCallback ie;
	 private boolean actionComplete;
	 
	 public EventNotifier (EventCallback event)
	 {
		 ie = event;
		 actionComplete = false;
	 }	 
	 
	 public void Signal(String action, Object obj)
	 {
		 actionComplete = true;
		 if(ie == null)
			 return;
		 ie.CallbackEvent(action, obj);
	 }
	 
	 public boolean getAction()
	 {
		 return actionComplete;
	 }
}
