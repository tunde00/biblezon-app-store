//AUTHOR:TUNDE OLAWUWO

package com.biblezon.appstore.notifier;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.support.v4.content.LocalBroadcastManager;

import com.biblezon.appstore.dbutil.Database;
import com.biblezon.appstore.util.ApkUtil;

import java.util.List;

public class PackageAddedReceiver extends BroadcastReceiver {
  
	@Override
	public void onReceive(Context context, Intent intent) {

		String nextAction = "";
		String action = intent.getAction();
		final ApkUtil apkutil = new ApkUtil(context);
		Handler handler = new Handler();

		final Context mycontext = context;
		final Database db = new Database(context);
		if (db == null) return;

		String[] a = intent.getData().toString().split(":");
		final String appPackageName = a[a.length - 1];
		final String appLanguage = db.getAPKLanguage(appPackageName);

		if (Intent.ACTION_PACKAGE_ADDED.equals(action))
		{
			db.RemoveApkDownloaded(appPackageName);

			   /*if(appPackageName.equalsIgnoreCase("com.app.biblelauncher"))
			   {
				   Intent SplashInitBackIntent = new Intent("SplashInitCallBack");
				   LocalBroadcastManager.getInstance(context).sendBroadcast(SplashInitBackIntent);
				   return;
			   }*/

			//attempt to create shortcut on the BibleZOn launcher
			handler.post(new Runnable() {
				@Override
				public void run() {

					if (appPackageName.equalsIgnoreCase("com.app.biblelauncher") ||
							appPackageName.equalsIgnoreCase("com.app.kids.launcher"))
						return;

					Drawable appIcon = null;
					String appName = "";
					String appClassName = "";

					List<PackageInfo> packageInfoList = mycontext.getPackageManager().getInstalledPackages(0);

					for (int i = 0; i < packageInfoList.size(); i++) {
						PackageInfo packageInfo = packageInfoList.get(i);
						if (packageInfo.packageName.equals(appPackageName)) {
							appName = packageInfo.applicationInfo.loadLabel(mycontext.getPackageManager()).toString();
							appIcon = packageInfo.applicationInfo.loadIcon(mycontext.getPackageManager());
							appClassName = packageInfo.applicationInfo.className;
						}
					}

					if (null == appClassName) {
						appClassName = mycontext.getPackageManager().getLaunchIntentForPackage(appPackageName).getComponent().getClassName();
					}

					apkutil.CreateAppShortcut(appPackageName, appName, appClassName, appIcon, appLanguage);
				}
			});

			nextAction = "INSTALL_COMPLETE";

		} else if (Intent.ACTION_PACKAGE_REMOVED.equals(action)) {
			nextAction = "UNINSTALL_COMPLETE";
		}

		//This will post to the app detail
		Intent AppdetailcallBackIntent = new Intent("AppDetailCallBack");
		AppdetailcallBackIntent.putExtra("Status", nextAction);
		LocalBroadcastManager.getInstance(context).sendBroadcast(AppdetailcallBackIntent);
	}
	
	
	
	
}
