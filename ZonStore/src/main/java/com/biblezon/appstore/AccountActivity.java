package com.biblezon.appstore;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.biblezon.appstore.fragment.AccountFragment;

/**
 * Created by tunde00 on 11/11/2015.
 */
public class AccountActivity extends AppCompatActivity implements View.OnClickListener {

    FragmentManager fm;
    AccountFragment app_frag;
    FragmentTransaction fragmentTransaction;

    Button account_register;
    Button account_login;
    Button account_back;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account);

        account_back = (Button)findViewById(R.id.btn_acct_back);
        account_login = (Button)findViewById(R.id.btn_acct_login);
        account_register = (Button)findViewById(R.id.btn_acct_reg);

        app_frag = new AccountFragment();
        fm = getSupportFragmentManager();
        fragmentTransaction = fm.beginTransaction();
        fragmentTransaction
                .replace(R.id.account_lay_1, app_frag, "SEARCHFRAG_TAG")
                .commit();
    }

    @Override
    public void onClick(View v) {

        switch(v.getId())
        {
            case R.id.btn_acct_login:
                break;
            case R.id.btn_acct_reg:
                break;
            case R.id.btn_acct_back:
                break;
        }
    }
}
