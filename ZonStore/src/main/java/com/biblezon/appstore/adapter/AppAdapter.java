package com.biblezon.appstore.adapter;

import android.app.Activity;
import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.BaseAdapter;
import android.widget.RatingBar;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.biblezon.appstore.R;
import com.biblezon.appstore.model.AppItem;
import com.biblezon.appstore.rpc.AppController;
import com.biblezon.appstore.util.ApkUtil;

import java.util.List;

/**
 * Created by tunde00 on 11/13/2015.
 */
public class AppAdapter extends BaseAdapter
{
    private LayoutInflater inflater;

    private ImageLoader imageLoader = AppController.getInstance().getImageLoader();
    private Object currentapps;
    List<AppItem> mAppsItem;
    ApkUtil apkUtility;

    private int mLastPosition;
    Activity activity;

    public AppAdapter(Activity activity, List<AppItem> mAppsItem)
    {
        this.activity = activity;
        this.mAppsItem = mAppsItem;
        this.apkUtility = new ApkUtil(activity);
        this.currentapps = apkUtility.getInstalledApps(false);
    }

    @Override
    public int getCount() {
        return mAppsItem.size();
    }

    @Override
    public Object getItem(int position) {
        return mAppsItem.get(position);
    }

    @Override
    public long getItemId(int arg0) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent)
    {
        final ViewHolder holder;
        if (convertView == null)
        {
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            convertView = inflater.inflate(R.layout.appsview_apps_item, parent, false);

            holder = new ViewHolder();
            holder.AppNameTxt = (TextView) convertView.findViewById(R.id.appName);
            //holder.AppDescriptionTxt = (TextView) convertView.findViewById(R.id.appDesc);
            holder.AppRating = (RatingBar) convertView.findViewById(R.id.appRating);
            holder.AppImg = (NetworkImageView) convertView.findViewById(R.id.appImg);
            holder.AppPricingTxt = (TextView) convertView.findViewById(R.id.appPricing);
            //	holder.DownloadProgressBar = (ProgressBar) convertView.findViewById(R.id.download_progress_bar);
            convertView.setTag(holder);
        }
        else
        {
            holder = (ViewHolder) convertView.getTag();
        }

        AppItem item = mAppsItem.get(position);
        holder.AppNameTxt.setText(item.getAppName());
        holder.AppImg.setImageUrl(item.getAppPix(), imageLoader);
        float rating = Float.parseFloat(item.getAppRating());
        holder.AppRating.setRating(rating);
        holder.AppPricingTxt.setText(item.getAppPrice());

        //determine if button should be Update (if Version is different), Free(default), Installed, no price introduced yet
        String AppVersionCode;
        if( item.getAppVersionCode().contains(".") )
            AppVersionCode = item.getAppVersionCode().substring(0,  item.getAppVersionCode().indexOf(".") ).trim();
        else
            AppVersionCode = item.getAppVersionCode().trim();
        String appCase =  apkUtility.getAppStatus(item.getAppPackageName(), Integer.parseInt(AppVersionCode), currentapps );

        if(appCase.equalsIgnoreCase("new"))
        {
            if(item.getAppPrice().equalsIgnoreCase("Free"))
                holder.AppPricingTxt.setText("FREE");
            else if(item.getAppPrice().equalsIgnoreCase("Inst") || item.getAppPrice().equalsIgnoreCase("Installed") )
                holder.AppPricingTxt.setText(Html.fromHtml("<font color='#008000'><b>INSTALLED</b></font>"));
            else
                holder.AppPricingTxt.setText("$" + item.getAppPrice());
        }
        if(appCase.equalsIgnoreCase("update")) holder.AppPricingTxt.setText(Html.fromHtml("<font color='#483D8B'><b>UPDATE</b></font>"));
        if(appCase.toLowerCase().startsWith("exist")) holder.AppPricingTxt.setText(Html.fromHtml("<font color='#008000'><b>INSTALLED</b></font>"));
        item.setAppStatus(appCase);

        // Do all your drawing, ViewHolder stuff, etc.
        // animate the item
        TranslateAnimation animation = null;
        if (position > mLastPosition)
        {
            animation = new TranslateAnimation( Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF, 1.0f, Animation.RELATIVE_TO_SELF, 0.0f);
            animation.setDuration(800);
            convertView.startAnimation(animation);
            mLastPosition = position;
        }

        return convertView;
    }


    class ViewHolder {
        TextView AppNameTxt;
        RatingBar AppRating;
        TextView AppPricingTxt;
        ImageLoader imageloader;
        NetworkImageView AppImg;
        TextView AppDescriptionTxt;
    }

}

