package com.biblezon.appstore.adapter;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.biblezon.appstore.R;
import com.biblezon.appstore.fragment.AppDetailFragment;
import com.biblezon.appstore.fragment.CategoryDetailFragment;
import com.biblezon.appstore.model.AppItem;
import com.biblezon.appstore.model.CategoryItem;
import com.biblezon.appstore.rpc.AppController;
import com.biblezon.appstore.util.ApkUtil;

import java.io.Serializable;
import java.util.List;

/**
 * Created by tunde00 on 11/13/2015.
 */
public class CategoryItemAdapter extends BaseAdapter
{
    private Activity activity;
    private int mLastPosition;
    private String defaultView;
    private LayoutInflater inflater;
    private List<CategoryItem> categoryItems;
    private ImageLoader imageLoader = AppController.getInstance().getImageLoader();

    FragmentManager fm;
    FragmentTransaction fragmentTransaction;

    public CategoryItemAdapter(Activity activity, List<CategoryItem> listcategory,
                               String whichView, FragmentManager fm)
    {
        this.fm = fm ;
        this.activity = activity;
        this.defaultView = "category";
        this.categoryItems = listcategory;

        if (whichView.equalsIgnoreCase("language"))
            defaultView = "language";
    }

    @Override
    public int getCount() {
        return categoryItems.size();
    }

    @Override
    public Object getItem(int position) {
        return categoryItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final ViewHolder holder;
        if (convertView == null)
        {
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            holder = new ViewHolder();

            if(defaultView.equalsIgnoreCase("category"))
            {
                convertView = inflater.inflate(R.layout.appsview_categories_item, parent, false);
                holder.CategoryNameTxt = (TextView) convertView.findViewById(R.id.tv_category_name);
                //holder.CategoryDescriptionTxt = (TextView) convertView.findViewById(R.id.tv_category_desc);
                holder.CategoryImg = (NetworkImageView) convertView.findViewById(R.id.tv_category_icon);
            }
            else
            {
                convertView = inflater.inflate(R.layout.appsview_languages_item, parent, false);
                holder.CategoryNameTxt = (TextView) convertView.findViewById(R.id.tv_language_name);
                //holder.CategoryDescriptionTxt = (TextView) convertView.findViewById(R.id.tv_language_desc);
                holder.CategoryImg = (NetworkImageView) convertView.findViewById(R.id.tv_language_icon);
            }
            convertView.setTag(holder);
        }
        else
        {
            holder = (ViewHolder) convertView.getTag();
        }

        final CategoryItem item = categoryItems.get(position);
        holder.CategoryNameTxt.setText(item.getCategoryName());
        //holder.CategoryDescriptionTxt.setText( Html.fromHtml(item.getCategoryDescription()));
        holder.CategoryImg.setImageUrl(item.getCategoryPictureUrl(), imageLoader);

        if(defaultView.equalsIgnoreCase("category"))
            holder.CategoryNameTxt.setText(item.getCategoryDispName());

        holder.CategoryImg.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View arg0) {

                Log.v("pos", "" + position);

                CategoryDetailFragment app_frag = new CategoryDetailFragment();
                Bundle b = new Bundle();
                CategoryItem item = categoryItems.get(position);

                b.putSerializable("data", (Serializable) item);
                app_frag.setArguments(b);

                String FragTAG = "CATEGORYDETAIL_FRAG";
                fragmentTransaction = fm.beginTransaction();
                fragmentTransaction.setCustomAnimations(
                        R.anim.translucent_enter,
                        R.anim.translucent_exit,
                        R.anim.translucent_enter,
                        R.anim.translucent_exit);
                fragmentTransaction.replace(R.id.lay_1, app_frag, FragTAG);
                fragmentTransaction.addToBackStack(FragTAG);
                fragmentTransaction.commit();

            }
        });


        // Do all your drawing, ViewHolder stuff, etc.
        // animate the item
        TranslateAnimation animation = null;
        if (position > mLastPosition)
        {
            animation = new TranslateAnimation( Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF, 1.0f, Animation.RELATIVE_TO_SELF, 0.0f);
            animation.setDuration(800);
            convertView.startAnimation(animation);
            mLastPosition = position;
        }

        return convertView;
    }

    static class ViewHolder
    {
        TextView CategoryNameTxt;
        NetworkImageView CategoryImg;
        ImageLoader imageloader;
    }

}