package com.biblezon.appstore.util;

/**
 * Created by tunde00 on 10/14/2015.
 */

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.telephony.TelephonyManager;

import java.util.ArrayList;
import java.util.List;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings.Secure;
import android.telephony.TelephonyManager;


public class DeviceManager {

    //GPS parameters
    private double mLatitude;
    private double mLongitude;
    private LocationManager lm;

    //device id
    private TelephonyManager mTelephony;

    private Activity invoketeer;


    public DeviceManager(Activity _activity)
    {
        invoketeer = _activity;
        mLatitude = 0;
        mLongitude = 0;
        mTelephony = (TelephonyManager)_activity.getSystemService(Context.TELEPHONY_SERVICE);
    }


    /////////////////////// CAPTURE DEVICE ID ///////////////////////////////////

    public String GetDeviceID()
    {
        String AndroidDeviceId = "";
        if (mTelephony.getDeviceId() != null){
            AndroidDeviceId = mTelephony.getDeviceId(); //*** use for mobiles
        }
        else{
            AndroidDeviceId = Settings.Secure.getString(invoketeer.getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID); //*** use for tablets
        }

        return AndroidDeviceId;
    }

    ////////////////////////////////////////////////////////////////////////////////


    /////////////////////////// WIFI ////////////////////////////////////////////////////

    public boolean isConnectedViaWifi()
    {
        ConnectivityManager cm = (ConnectivityManager) invoketeer.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        }
        return false;
    }

    ///////////////////////////////////////////////////////////////////////////////////////



    /////////////////////// GPS RELATED ACTIVITY /////////////////////////////////

    public boolean ActivateGPS()
    {
        boolean activated = false;  //using network because its a Wifi-only device

        try
        {
            lm = (LocationManager) invoketeer.getSystemService(Context.LOCATION_SERVICE);
            //turnGPSOn();
            if (lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER))
            {
                lm.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 2000, 10, GPSlocationlistener);
                activated = true;
            }
        }
        catch(SecurityException ex){}

        return activated;
    }

    public ArrayList<Double> GetGPSCoords(){

        try
        {
            if (mLatitude == 0) {
                Criteria criteria = new Criteria();
                criteria.setAccuracy(Criteria.ACCURACY_FINE);
                criteria.setPowerRequirement(Criteria.POWER_LOW);
                criteria.setAltitudeRequired(false);
                criteria.setBearingRequired(false);
                criteria.setSpeedRequired(false);
                criteria.setCostAllowed(true);

                String BBprovider = lm.getBestProvider(criteria, true);
                Location bestLocation = lm.getLastKnownLocation(BBprovider);

                if (bestLocation == null)
                    bestLocation = lm.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

                if (bestLocation == null) {
                    List<String> providers = lm.getProviders(false);
                    for (String provider : providers) {
                        Location l = lm.getLastKnownLocation(provider);
                        if (l == null) {
                            continue;
                        }
                        if (bestLocation == null || l.getAccuracy() < bestLocation.getAccuracy()) {
                            bestLocation = l;
                        }
                    }
                }

                if (bestLocation != null) {
                    mLatitude = bestLocation.getLatitude();
                    mLongitude = bestLocation.getLongitude();
                }
            }
        }
        catch (SecurityException ex)
        {

        }

        ArrayList<Double> coords = new ArrayList<Double>();
        coords.add(mLatitude);
        coords.add(mLongitude);

        return coords;
    }

    private final LocationListener GPSlocationlistener = new LocationListener(){

        @Override
        public void onLocationChanged(Location location) {
            mLatitude = location.getLatitude();
            mLongitude = location.getLongitude();
        }

        @Override
        public void onProviderDisabled(String provider) {}

        @Override
        public void onProviderEnabled(String provider) {}

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {}
    };

    @SuppressWarnings("unused")
    private void turnGPSOn(){

        if(!lm.isProviderEnabled(LocationManager.GPS_PROVIDER)){ //if gps is disabled
            final Intent poke = new Intent();
            poke.setClassName("com.android.settings", "com.android.settings.widget.SettingsAppWidgetProvider");
            poke.addCategory(Intent.CATEGORY_ALTERNATIVE);
            poke.setData(Uri.parse("3"));
            invoketeer.sendBroadcast(poke);
        }
    }

    ///////////////////////////////////////////////////////////////////////////////
}

