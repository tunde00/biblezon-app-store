package com.biblezon.appstore.util;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;

import com.biblezon.appstore.notifier.WidgetImageFetchComplete;

import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by tunde00 on 12/24/2015.
 */
public class AsyncImageWidgetDownload extends AsyncTask<List<String>, Void, List<Bitmap> > {

    private WidgetImageFetchComplete listener;

    public AsyncImageWidgetDownload( WidgetImageFetchComplete listener )
    {
        this.listener = listener;
    }

    @Override
    protected List<Bitmap> doInBackground(List<String>... lists)
    {
        List<Bitmap> mybitmaps = new ArrayList<>();

        for (String url : lists[0]) {
            Bitmap bm  = fetchImage(url);
            if(bm != null) {
                mybitmaps.add(bm);
            }
        }
        return mybitmaps;
    }

    private Bitmap fetchImage(String url) {

        Bitmap mIcon11 = null;
        try {

            URL req = new URL(url);
            mIcon11 = BitmapFactory.decodeStream(req.openConnection()
                    .getInputStream());

        } catch (Exception e) {
            Log.e("Error", e.getMessage());
            e.printStackTrace();
        }
        return mIcon11;
    }

    @Override
    protected void onPostExecute(List<Bitmap> resultBitmaps)
    {
        listener.onWidgetImageCompleted(resultBitmaps);
    }
}
