//AUTHOR: Olatunde Olawuwo

package com.biblezon.appstore.util;

import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.biblezon.appstore.dbutil.Database;
import com.biblezon.appstore.util.StorageUtility.StorageInfo;

import java.io.File;
import java.util.ArrayList;
import java.util.List;


public class ApkUtil {


	private static final String TAG = "utility_class";

	public ApkUtil(Context context){
		
			mContext = context;
			db = new Database(context);
		}
	 
		
		private Context mContext;
		private Database db;
		
	    class PInfo {
	        public String appname = "";
	        public String pname = "";
	        public String versionName = "";
	        public int versionCode = 0;
	        public boolean isUnInstallable = false;
	        public PInfo(){}
	    }	   
	   
	    
	    public  ArrayList<PInfo> getInstalledApps(boolean getSysPackages) 
	    {       
	        ArrayList<PInfo> res = new ArrayList<PInfo>();        
	        List<PackageInfo> packs = mContext.getPackageManager().getInstalledPackages(0);
	     		        
	        for(int i=0;i<packs.size();i++) 
	        {
				try {
					PackageInfo p = packs.get(i);
					if ((!getSysPackages) && (p.versionName == null)) {
						continue;
					}
					PInfo newInfo = new PInfo();
					newInfo.appname = p.applicationInfo.loadLabel(mContext.getPackageManager()).toString();
					newInfo.pname = p.packageName;
					newInfo.versionName = p.versionName;
					newInfo.versionCode = p.versionCode;

					if (p.applicationInfo.sourceDir.toLowerCase().startsWith("/system/"))
						newInfo.isUnInstallable = true;
					else {
						int mask = ApplicationInfo.FLAG_SYSTEM | ApplicationInfo.FLAG_UPDATED_SYSTEM_APP;
						newInfo.isUnInstallable = (p.applicationInfo.flags & mask) == 0;
					}

					res.add(newInfo);
				}
				catch(Exception ex)
				{
					Log.d(TAG, "getInstalledApps: " + ex.getMessage());
				}

	        }
	        return res; 
	    }
	    
	    public String getInstallPackageVersionInfo(String appName) 
	    {
	        String InstallVersion = "";     
	        ArrayList<PInfo> apps = getInstalledApps(false); /* false = no system packages */
	        final int max = apps.size();
	        for (int i=0; i<max; i++) 
	        {	              
	            if(apps.get(i).appname.toString().equals(appName.toString()))
	            {
	                InstallVersion = "Install Version Code: "+ apps.get(i).versionCode+
	                    " Version Name: "+ apps.get(i).versionName.toString();
	                break;
	            }
	        }

	        return InstallVersion.toString();
	    }
	    	    
	    public String getAppStatus(String appPackageName, int packageVersionCode)
	    {
	    	//RETURNS NEW for apps does not exist in the device
	    	//RETURNS UPDATE for it does exist in the device but with higher version
	    	//RETURNS EXISTS for package with same version existing in the device
	    	
	    	String status = "NEW";
	    	ArrayList<PInfo> apps = getInstalledApps(false); /* false = no system packages */
	    	final int max = apps.size();
		    for (int i=0; i<max; i++) 
		    {
		    	if(!apps.get(i).pname.toString().equals(appPackageName.toString())) continue;
		    	if(packageVersionCode > apps.get(i).versionCode) status = "UPDATE";
		    	else status = "EXISTS";
		    }
		    
		    return status;
	    }
	    
	    public String getAppStatus(String appPackageName, int packageVersionCode, Object appsObj)
	    {
	    	//RETURNS NEW for apps does not exist in the device
	    	//RETURNS UPDATE for it does exist in the device but with higher version
	    	//RETURNS EXISTS for package with same version existing in the device
	    	
	    	if(appsObj == null) 
	    		return null; 
	    			
	    	String status = "NEW";
	    	
	    	@SuppressWarnings("unchecked")
			ArrayList<PInfo> apps = (ArrayList<PInfo>) appsObj;
	    	
	    	final int max = apps.size();
		    for (int i=0; i<max; i++) 
		    {
		    	if(!apps.get(i).pname.toString().equals(appPackageName.toString())) continue;
		    	if(packageVersionCode > apps.get(i).versionCode) status = "UPDATE";
		    	else{
		    		status = "EXISTS";
		    		if(apps.get(i).isUnInstallable)
		    			status = "EXISTING SYS APPS";
		    	}
		    }		    
		    
			return status;	    	
	    }
	    
	    public void UnInstallApp(String packageName)// Specific package Name Uninstall.
	    {
	        Uri packageURI = Uri.parse("package:" + packageName.toString());
	        Intent uninstallIntent = new Intent(Intent.ACTION_DELETE, packageURI);
	        mContext.startActivity(uninstallIntent); 
	    }
	    
	   public void OpenApp(File sdcard, String ZonAppStoreDownloadedApk)
	   {
		   Intent intent = new Intent(Intent.ACTION_VIEW);
   		   intent.setDataAndType(Uri.fromFile(new File(sdcard, ZonAppStoreDownloadedApk)), "application/vnd.android.package-archive");
   		   intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK); // without this flag android returned a intent error!
   		   mContext.startActivity(intent);
	   }
	   
	   public  boolean openApp( String packageName) throws NameNotFoundException
	   {
		    PackageManager manager = mContext.getPackageManager();
		    Intent i = manager.getLaunchIntentForPackage(packageName);
			if (i == null) {
			    return false;
			}
			i.addCategory(Intent.CATEGORY_LAUNCHER);
			mContext.startActivity(i);
			return true;
		}
	    
	   
	   public void CreateAppShortcut(String packagename , String name, String classname, Drawable appicon, String language)
	   {
		
		  ApplicationInfo app = null;
		  Intent shortcutIntent = new Intent(); 


		  try {

			  if(language == null || language.length() < 1)
				  language = "English"; //default language to English

			  app = mContext.getPackageManager().getApplicationInfo(packagename, 0);
			  
			  if(classname.isEmpty()) classname = app.className;
			  if(name.isEmpty()) name = (String) mContext.getPackageManager().getApplicationLabel(app);
			  if(appicon == null) appicon = mContext.getPackageManager().getApplicationIcon(packagename);			  
			   
			  shortcutIntent.setClassName(packagename, classname );
			  shortcutIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			  shortcutIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		   } 
		   catch (NameNotFoundException e1) 
		   {
			   e1.printStackTrace();
			   return;
		   }
		   catch(Exception ex){
			  Toast.makeText(mContext, "Unable to determine newly added app details, Aborting!" 
					  	+ ex.getMessage(),  Toast.LENGTH_LONG).show();
			  return;
		   }
		   
		   android.graphics.drawable.BitmapDrawable bd = (android.graphics.drawable.BitmapDrawable) appicon;
		  		   
		   Intent addIntent = new Intent();
		   addIntent.putExtra(Intent.EXTRA_SHORTCUT_INTENT, shortcutIntent);
		   addIntent.putExtra(Intent.EXTRA_SHORTCUT_NAME, name);
		   addIntent.putExtra(Intent.EXTRA_SHORTCUT_ICON, bd.getBitmap());
		   // addIntent.setAction("com.android.launcher.action.INSTALL_SHORTCUT");

		   addIntent.putExtra("android.intent.extra.shortcut.PACKAGE_NAME", packagename);
		   addIntent.putExtra("android.intent.extra.shortcut.SHORTCUT_ACTION", "ADD");
		   addIntent.putExtra("android.intent.extra.shortcut.APP_LANGUAGE", language);
		   addIntent.setAction("android.intent.action.BIBLEZON_SHORTCUT");
		   
		   mContext.sendBroadcast(addIntent);

	   }



	  public void DownloadAPKWithoutDownloadManager(String url, String title, String packagename, View v, String filename)
	  {
		  AsyncAppDownloader AsyncAppDownloader = new AsyncAppDownloader();
		  AsyncAppDownloader.setContext(mContext);
		  AsyncAppDownloader.execute(url, filename);
	  }


	   //Downloads APK from source url using download manager 
	   public void DownloadAPK(String url, String title, String packagename, View v, String filename, String applanguage)
	   {
		   //for handling the after event of the download action by pointing to the handler
		   IntentFilter filter = new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE);		
		   mContext.registerReceiver(downloadReceiver, filter);
		   sourceView = v;

		   //determine dirType
		   String externalStoragePath = Environment.DIRECTORY_DOWNLOADS;
		   List<StorageInfo> storagePathInfo  = StorageUtility.getStorageList();		
			if(storagePathInfo.size() > 0)
				externalStoragePath = storagePathInfo.get(0).path + "/ZonAppStore";
			 
		   //Prepare the download request 	   
		   DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));
		   request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE )
		   		  .setAllowedOverRoaming(false)
		   		  .setDescription(packagename)
		   		  .setTitle(title)
		   		  .setDestinationInExternalFilesDir(mContext, externalStoragePath, filename);
				  		   
		   // in order for this if to run, you must use the android 3.2 to compile your app
		   if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) 
		   {
		       request.allowScanningByMediaScanner();
		       request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
		   }


		   //hold the language of the app being downloaded
		   //so as to reference it when notifying the launcher
		   if( !db.APKExisit(packagename) )
		   {
			   db.AddAPK(title,packagename, "1", applanguage );
		   }

		   
		   // get download service and enqueue file
		   downloadManager = (DownloadManager) mContext.getSystemService(Context.DOWNLOAD_SERVICE);
		   downloadReference = downloadManager.enqueue(request);		        
	   }

	   private View sourceView;
	   private long downloadReference;
	   private DownloadManager downloadManager;
	   
	   //handles to process downloaded file
	   public BroadcastReceiver downloadReceiver = new BroadcastReceiver() 
	   {
		   public void onReceive(Context context, Intent intent) 
		   {
			   long referenceId = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1);
			   if(downloadReference != referenceId)
			   {
				   Log.v(TAG,"Ignoring unrelated download" + referenceId);
			   }

			   DownloadManager downloadManager = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
			   DownloadManager.Query query = new DownloadManager.Query();
			   query.setFilterById(referenceId);

			   Cursor cursor = downloadManager.query(query);

			    // it shouldn't be empty, but just in case
			   if (!cursor.moveToFirst()) {
				   Log.e(TAG, "Empty row");

				   Intent AppdetailcallBackIntent = new Intent("AppDetailCallBack");
				   AppdetailcallBackIntent.putExtra("Status", "DOWNLOAD_FAILED");
				   LocalBroadcastManager.getInstance(context).sendBroadcast(AppdetailcallBackIntent);
				   return;
			   }

			   int statusIndex = cursor.getColumnIndex(DownloadManager.COLUMN_STATUS);
			   if (DownloadManager.STATUS_SUCCESSFUL != cursor.getInt(statusIndex)) {
				   Log.w(TAG, "Download Failed");

				   Intent AppdetailcallBackIntent = new Intent("AppDetailCallBack");
				   AppdetailcallBackIntent.putExtra("Status", "DOWNLOAD_FAILED");
				   LocalBroadcastManager.getInstance(context).sendBroadcast(AppdetailcallBackIntent);

				   return;
			   }

			   int uriIndex = cursor.getColumnIndex(DownloadManager.COLUMN_LOCAL_URI);
			   String downloadedPackageUriString = cursor.getString(uriIndex);

			   Intent installIntent = new Intent(Intent.ACTION_VIEW);
			   installIntent.setDataAndType(downloadManager.getUriForDownloadedFile(downloadReference), "application/vnd.android.package-archive");
			   installIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			   mContext.unregisterReceiver(downloadReceiver);

   			   mContext.startActivity(installIntent);
			   if(sourceView != null) {
				   if (sourceView instanceof Button) {
					   Button btn = ((Button) sourceView);
					   btn.setText("Installing.... ");
					   //btn.setEnabled(false);
				   }
			   }

		   }
	   };
}
