package com.biblezon.appstore.util;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.NotificationCompat;

public class NotificationHelper {
	
    private Context mContext;
    private int NOTIFICATION_ID = 1;
    private Notification mNotification;
    private NotificationManager mNotificationManager;
    private PendingIntent mContentIntent;
    private CharSequence mContentTitle;
    
    private String mTickerText;
    private String mTitleText;
    
    public NotificationHelper(Context context, String title, String ticker)
    {
        mContext = context;
        mTitleText = title;
        mTickerText = ticker;
    }
 
    /**
     * Put the notification into the status bar
     */
    public void createNotification() {
    	
        //get the notification manager
        mNotificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
 
        //create the notification      
        CharSequence tickerText = mTickerText;

        //create the content which is shown in the notification pulldown
        mContentTitle = mTitleText; //Full title of the notification in the pull down
        CharSequence contentText = "0% complete"; //Text of the notification in the pull down
 
        //you have to set a PendingIntent on a notification to tell the system what you want it to do when the notification is selected
        //I don't want to use this here so I'm just creating a blank one
        Intent notificationIntent = new Intent();
        mContentIntent = PendingIntent.getActivity(mContext, 0, notificationIntent, 0);
 
        //add the additional content and intent to the notification
        NotificationCompat.Builder builder = new NotificationCompat.Builder(mContext);
        mNotification = builder.setContentIntent(mContentIntent)
                .setSmallIcon(android.R.drawable.stat_sys_download)
                .setTicker(tickerText)
                .setWhen(System.currentTimeMillis())
                .setAutoCancel(true)
                .setContentTitle(mContentTitle)
                .setContentText(contentText).build();

        //make this notification appear in the 'Ongoing events' section
        mNotification.flags = Notification.FLAG_ONGOING_EVENT;

        //set custom sound for notification
        //mNotification.sound = Uri.parse("android.resource://" + context.getPackageName() + "your_sound_file_name.mp3");
 
        //show the notification
        mNotificationManager.notify(NOTIFICATION_ID, mNotification);
    }
 

    public void progressUpdate(int percentageComplete) {
        
    	//build up the new status message
        CharSequence contentText = percentageComplete + "% complete";
        mNotification.contentIntent = mContentIntent;
        mNotification.tickerText = contentText;
        mNotificationManager.notify(NOTIFICATION_ID, mNotification);
    }

    public void completed()    {
        //remove the notification from the status bar
        mNotificationManager.cancel(NOTIFICATION_ID);
    }
}
