package com.biblezon.appstore.autoupdate;

import android.app.Activity;
import android.app.DownloadManager;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import java.util.ArrayList;


public class AutoUpdate {

	private String appName;
	private String apkFileName;
	private Activity mainActivity;
	private VSWebReceiver receiver;
	private static final String LOG_TAG = "AppUpgrade";
	private String UPDATE_SOURCE_URL;  //e.g."http://biblezon.visionsoftwareonline.com/apk/checkversion.php?app=LiturgicalCalendar"
	private String PROCESS_RESPONSE;   //e.g. "com.biblezon.prayercommunity.intent.action.PROCESS_RESPONSE"
	
	
	private static AutoUpdate mInstance;
	
	private AutoUpdate(Activity _mainActivity, String upgrade_url, String _processresponse, String _appName, String _apkFileName)
	{
		appName = _appName;
		apkFileName = _apkFileName;
		mainActivity = _mainActivity;
		UPDATE_SOURCE_URL = upgrade_url;
		PROCESS_RESPONSE = _processresponse;
	}
	
	public static synchronized  AutoUpdate getInstance(Activity ctx, String upgrade_url, String _processresponse, String _appName, String  _apkFileName) 
	{
		if (null == mInstance) { mInstance = new AutoUpdate(ctx, upgrade_url, _processresponse, _appName, _apkFileName); }
		return mInstance;
	}
		
	//Check for Updates
	public void performCheckforUpgrade()
	{
		//Broadcast receiver for our Web Request 
		IntentFilter filter = new IntentFilter(PROCESS_RESPONSE);
		filter.addCategory(Intent.CATEGORY_DEFAULT);
			  
		receiver = new VSWebReceiver(mainActivity, apkFileName, appName + " App Download");
		mainActivity.registerReceiver(receiver, filter);
			 
		filter = new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE);
		mainActivity.registerReceiver(receiver.downloadReceiver, filter);
			 
		if(isNetworkAvailable())
		{
			   Intent msgIntent = new Intent(mainActivity.getApplicationContext(), VSCheckAppVersionWS.class);
			   
			   ArrayList<String> param = new ArrayList<String>();
			   param.add(UPDATE_SOURCE_URL);
			   param.add(PROCESS_RESPONSE);
			   msgIntent.putStringArrayListExtra(VSCheckAppVersionWS.REQUEST_STRING, param);			   
			   
			   mainActivity.startService(msgIntent);
		}
	}

	private boolean isNetworkAvailable()
	{
		 ConnectivityManager connectivity = (ConnectivityManager) mainActivity.getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
		 if (connectivity != null) 
		 {
			 NetworkInfo[] info = connectivity.getAllNetworkInfo();
			 if (info != null) 
			 {
				 for (int i = 0; i < info.length; i++) 
				 {
					 Log.v(LOG_TAG,String.valueOf(i));
					 
					 if (info[i].getState() == NetworkInfo.State.CONNECTED) {
						 Log.v(LOG_TAG, "connected!");
						 return true;
					 }
				 }
			 }
		 }
		 return false;
	}
}
