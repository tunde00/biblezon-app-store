package com.biblezon.appstore.autoupdate;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

public class VSWebReceiver extends BroadcastReceiver{
	 
	 private static final String LOG_TAG = "AppUpgrade";
	  private int versionCode = 0;
	  
	  private String downloadtitle;
	  private long downloadReference;
	  private DownloadManager downloadManager;
	  
	  private String appURI = "";
	  private String apkFileName;
	  private Activity mainActivity;
	  
	  
	  public VSWebReceiver(Activity _mainActivity, String _apkfileName, String _downloadtitle){
		  apkFileName = _apkfileName;
		  mainActivity = _mainActivity;
		  downloadtitle = _downloadtitle; //"My Andorid App Download"
		  
		  
		  try {
			versionCode = _mainActivity.getPackageManager().getPackageInfo(_mainActivity.getPackageName(), 0).versionCode;
		  } catch (NameNotFoundException e) {
			e.printStackTrace();
		  }
	  }
	  
	  
	  @Override
	  public void onReceive(final Context context, Intent intent)
	  {
		   String reponseMessage = intent.getStringExtra(VSCheckAppVersionWS.RESPONSE_MESSAGE);
		   Log.v(LOG_TAG, reponseMessage);
		 
		   //parse the JSON response
		   JSONObject responseObj;
		   
		   try 
		   {	   
			    responseObj = new JSONObject(reponseMessage);
			    boolean success = responseObj.getBoolean("success");
			    
			    //if the reponse was successful check further
			    if(success){
		     
			    	//get the latest version from the JSON string
			    	int latestVersion = responseObj.getInt("latestVersion");
		    
			    	//get the lastest application URI from the JSON string
			    	appURI = responseObj.getString("appURI");
			    	
				    //check if we need to upgrade?
				    if(latestVersion > versionCode)
				    {
				    	//oh yeah we do need an upgrade, let the user know send an alert message
				    	AlertDialog.Builder builder = new AlertDialog.Builder(context);
				    	builder.setMessage("There is newer version of this application available, click OK to upgrade now?")
				    		.setPositiveButton("OK", 
				    				new DialogInterface.OnClickListener() {
				    					
				    					//if the user agrees to upgrade
				    					public void onClick(DialogInterface dialog, int id) {
				    						
									        //start downloading the file using the download manager
									        downloadManager = (DownloadManager)mainActivity.getSystemService(Context.DOWNLOAD_SERVICE);
									        
									        Uri Download_Uri = Uri.parse(appURI);
									        DownloadManager.Request request = new DownloadManager.Request(Download_Uri);
											request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_MOBILE|DownloadManager.Request.NETWORK_WIFI);
											request.setAllowedOverRoaming(false);
									        request.setTitle(downloadtitle);
											request.setShowRunningNotification(true);
											request.setDestinationInExternalFilesDir(mainActivity.getApplicationContext(),Environment.DIRECTORY_DOWNLOADS, apkFileName);
									        
									        downloadReference = downloadManager.enqueue(request);
				    					}
				        })
						.setNegativeButton("CANCEL",
								new DialogInterface.OnClickListener() {

									@Override
									public void onClick(DialogInterface dialogInterface, int i) {
										Toast.makeText(context, "Please restart the Store App to auto-update next time",
												Toast.LENGTH_LONG);
										mainActivity.unregisterReceiver(downloadReceiver);
								}
								});
	
				        //show the alert message
				        builder.create().show();
				    }
			    }
		   } catch (JSONException e){
			   e.printStackTrace();
			   mainActivity.unregisterReceiver(downloadReceiver);
		   }
		   finally{
			   mainActivity.unregisterReceiver(this);
		   }
	  }
	  
	  	  
	  public BroadcastReceiver downloadReceiver = new BroadcastReceiver() 
	  {
		  public void onReceive(Context context, Intent intent) {
		
			  //check if the broadcast message is for our Enqueued download
			  long referenceId = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1);
			  if(downloadReference == referenceId){
				  
				  Log.v(LOG_TAG, "Downloading of the new app version complete");
		    
				  //start the installation of the latest version
				  Intent installIntent = new Intent(Intent.ACTION_VIEW);
				  installIntent.setDataAndType(downloadManager.getUriForDownloadedFile(downloadReference),"application/vnd.android.package-archive");
				  installIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				  mainActivity.unregisterReceiver(downloadReceiver);
				  mainActivity.startActivity(installIntent);
			  }
		  	}
	}; 

	
}