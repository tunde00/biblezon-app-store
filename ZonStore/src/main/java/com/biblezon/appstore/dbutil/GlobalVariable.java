package com.biblezon.appstore.dbutil;

import android.app.Application;
import android.content.SharedPreferences;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class GlobalVariable extends Application {
	
	public static final String BOOLEAN_KEY_1 ="bol_key_1";
	
	public static final String BOOLEAN_KEY_FIRST_LAUNCH ="bol_key_frst";
	
	public static final String S_KEY_COLOR ="s_key_color";
	
	public static final String I_KEY_COLOR ="i_key_color";
	
	public static final String S_KEY_TABLE ="s_key_table";
	
	public static final String I_KEY_FAV_COUNTE ="i_key_fav_count";

	/**
	 * Universal shared preference
	 * for boolean
	 */
	public boolean getBooleanPref(String key_val, boolean def_val) {
		SharedPreferences pref = getSharedPreferences("pref_"+key_val,MODE_PRIVATE);
		return pref.getBoolean(key_val, def_val);
	}

	public void setBooleanPref(String key_val, boolean val) {
		SharedPreferences pref = getSharedPreferences("pref_"+key_val,MODE_PRIVATE);
		SharedPreferences.Editor prefEditor = pref.edit();
		prefEditor.clear();
		prefEditor.putBoolean(key_val, val);
		prefEditor.commit();
	}

	/**
	 * Universal shared preference
	 * for integer
	 */
	public int getIntPref(String key_val, int def_val) {
		SharedPreferences pref = getSharedPreferences("pref_"+key_val,MODE_PRIVATE);
		return pref.getInt(key_val, def_val);
	}

	public void setIntPref(String key_val, int val) {
		SharedPreferences pref = getSharedPreferences("pref_"+key_val,MODE_PRIVATE);
		SharedPreferences.Editor prefEditor = pref.edit();
		prefEditor.clear();
		prefEditor.putInt(key_val, val);
		prefEditor.commit();
	}

	/**
	 * Universal shared preference
	 * for string
	 */
	public String getStringPref(String key_val, String def_val) {
		SharedPreferences pref = getSharedPreferences("pref_"+key_val,MODE_PRIVATE);
		return pref.getString(key_val, def_val);
	}

	public void setStringPref(String key_val, String val) {
		SharedPreferences pref = getSharedPreferences("pref_"+key_val,MODE_PRIVATE);
		SharedPreferences.Editor prefEditor = pref.edit();
		prefEditor.clear();
		prefEditor.putString(key_val, val);
		prefEditor.commit();
	}


	public String generateCurrentDate(int format_key){
		Date curDate = new Date();
		String DateToStr = "";
		//default 11-5-2014 11:11:51
		SimpleDateFormat format = new SimpleDateFormat("dd-M-yyyy hh:mm:ss");

		switch (format_key) {
		case 1:

			format = new SimpleDateFormat("dd/MM/yyy");
			DateToStr = format.format(curDate);
			break;

		case 2:
			//May 11, 2014 11:37 PM
			DateToStr = DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.SHORT).format(curDate);
			break;
		case 3:
			//11-5-2014 11:11:51
			format = new SimpleDateFormat("dd-M-yyyy hh:mm:ss");
			DateToStr = format.format(curDate);
			break;
		}
		return DateToStr;
	}

}
