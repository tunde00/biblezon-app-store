package com.biblezon.appstore.dbutil;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;


public class Database extends SQLiteAssetHelper {

	private static final String DATABASE_NAME = "zonstoreapp.db";
	private static final int DATABASE_VERSION = 1;
	private static final String TABLE = "apps";

	public Database(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}


	public void AddAPK(String appName,String pkgName,String appVersion, String appLang)
	{
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put("app_name",appName);
		values.put("app_pkg_name", pkgName);
		values.put("app_version", appVersion);
		values.put("app_language", appLang);

		// Inserting Row
		db.insert("apps", null, values);
		db.close(); // Closing database connection

	}

	public boolean APKExisit(String apk_package) {
		SQLiteDatabase db = this.getReadableDatabase();

		Cursor cursor = db.query(TABLE,
				new String[] {"app_pkg_name" },	"app_pkg_name" + "=?",
				new String[] { apk_package }, null, null, null, null);

		if (cursor != null && cursor.getCount()>0){
			return true;
		}
		return false;
	}

	public boolean RemoveApkDownloaded(String apk_package)
	{
		SQLiteDatabase db = this.getReadableDatabase();

		int id = 0;
		String selectQuery = "SELECT _id FROM " + TABLE  +" where app_pkg_name='"+ apk_package +"' ORDER BY app_name ASC";

		Cursor cursor = db.rawQuery(selectQuery, null);
		if (cursor.moveToFirst())
		{
			id = cursor.getInt(0);
		}

		return db.delete(TABLE, "_id= " + id, null) > 0;
	}

	public String getAPKLanguage(String apk_package)
	{
		String language = "English";
		String selectQuery = "SELECT app_language FROM " + TABLE  +" where app_pkg_name='"+ apk_package +"' ORDER BY app_name ASC";
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		if (cursor.moveToFirst()) {
			language = cursor.getString(0);
		}

		return language;
	}

	// Getting All Contacts
	public ArrayList<String> getAllAPKs(String language) {
		ArrayList<String> noteList = new ArrayList<String>();
		// Select All Query
		String selectQuery = "SELECT  * FROM " + TABLE  +" where app_language='"+ language +"' and app_pkg_name is not null ORDER BY app_name ASC";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				noteList.add(cursor.getString(4));
			} while (cursor.moveToNext());
		}

		// return contact list
		return noteList;
	}


}
