package com.biblezon.appstore.installer;

public interface OnInstalledPackaged {
	
	public void packageInstalled(String packageName, int returnCode);

}
