package com.biblezon.appstore.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;

import com.biblezon.appstore.R;
import com.biblezon.appstore.adapter.AppAdapter;
import com.biblezon.appstore.adapter.CategoryItemAdapter;
import com.biblezon.appstore.model.AppItem;
import com.biblezon.appstore.model.CategoryItem;

import java.io.Serializable;
import java.util.List;

/**
 * Created by tunde00 on 11/11/2015.
 */
public class SeeMoreFragment extends Fragment {
    TextView headerTv;
    GridView gridView;

    String title = "";
    String type = "";

    private List<AppItem> mAppsItemRecommended;
    private List<CategoryItem> mAppsItemCat;

    FragmentManager fm;
    FragmentTransaction fragmentTransaction;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View mView = inflater.inflate(R.layout.see_more, container, false);
        fm = getActivity().getSupportFragmentManager();
        getArgumentValues();
        init(mView);
        setValues();
        return mView;
    }

    private void setValues()
    {
        headerTv.setText(title);

        if (type.equals("featured") || type.equals("top")) {
            gridView.setAdapter(new AppAdapter(getActivity(), mAppsItemRecommended));
        }
        else if (type.equals("cat")) {
            gridView.setAdapter( new CategoryItemAdapter(getActivity(), mAppsItemCat, "category", fm));
        } else {
            gridView.setAdapter(new CategoryItemAdapter(getActivity(), mAppsItemCat, "language", fm));
        }
    }

    private void getArgumentValues()
    {
        Bundle b = getArguments();

        title = b.getString("title");

        type = b.getString("type");

        if (type.equals("featured") || type.equals("top"))
            mAppsItemRecommended = (List<AppItem>) b.getSerializable("data");
        else
            mAppsItemCat = (List<CategoryItem>) b.getSerializable("data");

    }

    private void init(View mView) {

        headerTv = (TextView) mView.findViewById(R.id.textViewHeader);

        gridView = (GridView) mView.findViewById(R.id.gridview);

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int pos,long arg3)
            {
                Log.v("pos", "" + pos);
                Fragment app_frag = null;
                Bundle b = new Bundle();

                if (type.equals("featured") || type.equals("top"))
                {
                    AppItem item = mAppsItemRecommended.get(pos);
                    b.putSerializable("data", (Serializable) item);
                    app_frag = new AppDetailFragment();
                    app_frag.setArguments(b);
                }
                else
                {
                    CategoryItem item = mAppsItemCat.get(pos);
                    b.putSerializable("data", (Serializable) item);
                    app_frag = new CategoryDetailFragment();
                    app_frag.setArguments(b);
                }

                if(app_frag == null)
                    return;

                String FragTAG = "SEEMORESOURCE_TAG";
                fragmentTransaction = fm.beginTransaction();
                fragmentTransaction.setCustomAnimations(
                        R.anim.slide_in_from_top,
                        R.anim.slide_out_to_bottom,
                        R.anim.slide_in_from_top,
                        R.anim.slide_out_to_bottom);

                fragmentTransaction.replace(R.id.lay_1, app_frag,FragTAG );
                fragmentTransaction.addToBackStack(FragTAG);
                fragmentTransaction.commit();
            }
        });
    }
}
