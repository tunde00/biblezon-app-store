package com.biblezon.appstore.fragment;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.biblezon.appstore.R;
import com.biblezon.appstore.adapter.AppAdapter;
import com.biblezon.appstore.adapter.CategoryItemAdapter;
import com.biblezon.appstore.model.AppItem;
import com.biblezon.appstore.model.CategoryItem;
import com.biblezon.appstore.notifier.WidgetImageFetchComplete;
import com.biblezon.appstore.rpc.AppController;
import com.biblezon.appstore.rpc.VolleyErrorHelper;
import com.biblezon.appstore.util.ApkUtil;
import com.biblezon.appstore.util.AsyncImageWidgetDownload;
import com.biblezon.appstore.view.AutoScrollViewPager;
import com.biblezon.appstore.widget.ImagePagerAdapter;
import com.viewpagerindicator.CirclePageIndicator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by tunde00 on 11/11/2015.
 */
public class MainFragment extends Fragment implements View.OnClickListener, WidgetImageFetchComplete {

    GridView gridviewRec;
    GridView gridviewTop;
    GridView gridviewLang;
    GridView gridviewCat;

    Button seeMoreFeatured ;
    Button seeMoreTopApp ;
    Button seeMoreLang ;
    Button seeMoreCat ;

    private List<AppItem> mAppsItemRecommended;
    private List<AppItem> mAppsItemTops;
    private List<CategoryItem> mAppsItemLang;
    private List<CategoryItem> mAppsItemCat;

    private AppAdapter mRECAppsDisplayAdapter;
    private AppAdapter mTOPAppsDisplayAdapter;

    private View mView;
    FragmentManager fm;
    FragmentTransaction fragmentTransaction;

    private static int RecApp_Position;
    private static int TopApp_Position;
    private static final String ACTION_CALLBACK_PAGE = "MainFragmentViewCallBack";

    private AutoScrollViewPager WidgetViewPager;
    private CirclePageIndicator WidgetIndicator;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        if(mView == null)
        {
            mView = inflater.inflate(R.layout.main_frag, container, false);

            InitializeControls(mView);

            InitializeWidgetViewPager(mView);

            InitializeEventHandling();

            FetchDataContent();
        }

        ViewGroup parent = (ViewGroup) mView.getParent();
        if (parent != null) {
            parent.removeView(mView);
        }

        return mView;
    }

    private void InitializeControls(View mView)
    {
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(
                UpdateGridListReceiver, new IntentFilter(ACTION_CALLBACK_PAGE));

        fm = getActivity().getSupportFragmentManager();

        gridviewRec = (GridView) mView.findViewById(R.id.gridviewRec);
        gridviewTop = (GridView) mView.findViewById(R.id.gridviewTop);
        gridviewLang  = (GridView) mView.findViewById(R.id.gridviewLang);
        gridviewCat  = (GridView) mView.findViewById(R.id.gridviewCat);

        seeMoreFeatured = (Button) mView.findViewById(R.id.btn_featured);
        seeMoreTopApp = (Button) mView.findViewById(R.id.btn_top);
        seeMoreCat = (Button) mView.findViewById(R.id.btn_cat);
        seeMoreLang = (Button) mView.findViewById(R.id.btn_lang);

        mAppsItemRecommended =  new ArrayList<AppItem>();
        mAppsItemTops = new ArrayList<AppItem>();
        mAppsItemLang = new ArrayList<CategoryItem>();
        mAppsItemCat = new ArrayList<CategoryItem>();

        mRECAppsDisplayAdapter = new AppAdapter(getActivity(), mAppsItemRecommended);
        mTOPAppsDisplayAdapter = new AppAdapter(getActivity(), mAppsItemTops);
        gridviewRec.setAdapter(mRECAppsDisplayAdapter);
        gridviewTop.setAdapter(mTOPAppsDisplayAdapter);
    }

    private void InitializeEventHandling()
    {
        seeMoreFeatured.setOnClickListener(this);
        seeMoreTopApp.setOnClickListener(this);
        seeMoreLang.setOnClickListener(this);
        seeMoreCat.setOnClickListener(this);

        gridviewRec.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                if (position < 0)
                    return;

                TopApp_Position = 0;
                RecApp_Position = position;
                AppItem item = mAppsItemRecommended.get(position);
                item.setRequestedUIPage(ACTION_CALLBACK_PAGE);

                Log.v("rec app pos", "" + position);
                AppDetailFragment app_frag = new AppDetailFragment();
                Bundle b = new Bundle();
                b.putSerializable("data", (Serializable) item);
                app_frag.setArguments(b);

                String FragTAG = "APPDETAIL_FRAG";
                fragmentTransaction = fm.beginTransaction();
                fragmentTransaction
                        .setCustomAnimations(R.anim.translucent_enter, R.anim.translucent_exit,R.anim.translucent_enter, R.anim.translucent_exit)
                        .replace(R.id.lay_1, app_frag, FragTAG)
                        .addToBackStack(FragTAG)
                        .commit();
            }
        });


        gridviewTop.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                RecApp_Position = 0;
                TopApp_Position = position;
                AppItem item = mAppsItemTops.get(position);
                item.setRequestedUIPage(ACTION_CALLBACK_PAGE);

                Log.v("top app pos", "" + position);
                AppDetailFragment app_frag = new AppDetailFragment();
                Bundle b = new Bundle();
                b.putSerializable("data", (Serializable) item);
                app_frag.setArguments(b);

                String FragTAG = "APPDETAIL_FRAG";
                fragmentTransaction = fm.beginTransaction();
                fragmentTransaction
                        .setCustomAnimations(R.anim.translucent_enter, R.anim.translucent_exit,R.anim.translucent_enter, R.anim.translucent_exit)
                        .replace(R.id.lay_1, app_frag, FragTAG)
                        .addToBackStack(FragTAG)
                        .commit();
            }
        });
    }

    private void InitializeWidgetViewPager(View v)
    {
        WidgetViewPager = (AutoScrollViewPager)v.findViewById(R.id.recommended_widget_view_pager);
        WidgetIndicator = (CirclePageIndicator)v.findViewById(R.id.recommended_widget_indicator);
    }

    private void FetchDataContent()
    {
        // Tag used to cancel the request
        final String tag_json_obj = "mainfragment_json_req";
        String URL = getActivity().getResources().getString(R.string.app_featured_url);

        final ProgressDialog pDialog = new ProgressDialog(this.getActivity());
        pDialog.setMessage("loading...");
        pDialog.show();

        HashMap<String, String> _params = null;
        JSONObject jsonParams = null != _params ? new JSONObject(_params) :	null;

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                URL, jsonParams,
                new Response.Listener<JSONObject>()
                {
                    @Override
                    public void onResponse(JSONObject response)
                    {
                        VolleyLog.d(tag_json_obj, response.toString());
                        pDialog.hide();

                        if(response == null)
                        {
                            new AlertDialog.Builder(getActivity())
                                    .setMessage(Html.fromHtml("Unable to reach App server at this time <br>Please try again later!"))
                                    .show();
                            return;
                        }

                        ProcessServerResponse(response);
                    }
                },
                new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error)
                    {
                        VolleyLog.d(tag_json_obj, "Error: " + error.getMessage());
                        pDialog.hide();

                        new AlertDialog.Builder(getActivity())
                                .setMessage(Html.fromHtml("Oops experiencing network issues <br>" + VolleyErrorHelper.getMessage(error,getActivity()) ))
                                .show();
                    }
                }
        );

        int socketTimeout = 60000;//60 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        jsonObjReq.setRetryPolicy(policy);

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);


        //list of images. Always three constant images
        List<String> imageUrlList = new ArrayList<String>();
        imageUrlList.add(getResources().getString(R.string.widget_image_a));
        imageUrlList.add(getResources().getString(R.string.widget_image_b));
        imageUrlList.add(getResources().getString(R.string.widget_image_c));
        new AsyncImageWidgetDownload(this).execute(imageUrlList);
    }

    private void ProcessServerResponse(final JSONObject response)
    {
        try
        {
            JSONArray appsArray = response.getJSONArray("featured_items");

            for (int i = 0; i < appsArray.length(); i++)
            {
                JSONObject appObj = (JSONObject) appsArray.get(i);
                String featured_Type = appObj.getString("type"); //i.e. Recommended, Top, Category, Language

                switch(featured_Type)
                {
                    case "Recommended":
                    case "Top":

                        AppItem item = new AppItem();

                        item.setAppID(appObj.getInt("id"));
                        item.setAppName(appObj.getString("name")); //label of the app
                        item.setAppDesc(appObj.getString("desc"));
                        item.setAppSummaryDesc(appObj.getString("summary"));
                        item.setAppRating(appObj.getString("rating"));
                        item.setAppPix(appObj.getString("thumb_url"));

                        item.setAppSourceFileURL(appObj.getString("source_url"));
                        item.setAppPackageName(appObj.getString("packagename"));
                        item.setAppVerCode(appObj.getString("versioncode"));
                        item.setAppFileName(appObj.getString("filename"));

                        if(!appObj.isNull("language"))
                        {
                            item.setAppLanguage(appObj.getString("language"));
                        }

                        if(!appObj.isNull("pricing"))
                        {
                            item.setAppPrice( appObj.getString("pricing").equalsIgnoreCase("0") ? "Free" :
                                    appObj.getString("pricing") );
                        }

                        if (featured_Type.equalsIgnoreCase("Recommended"))
                            mAppsItemRecommended.add(item);

                        if (featured_Type.equalsIgnoreCase("Top"))
                            mAppsItemTops.add(item);

                        break;

                    case "Category":
                    case "Language":

                        CategoryItem categoryitem  = new CategoryItem();
                        categoryitem.setCategoryType(featured_Type);
                        categoryitem.setCategoryID(appObj.getInt("id"));
                        categoryitem.setCategoryName(appObj.getString("name"));
                        categoryitem.setCategoryDisplayName(appObj.getString("name"));
                        categoryitem.setCategoryDescription(appObj.getString("desc"));
                        categoryitem.setCategoryPictureUrl(appObj.getString("thumb_url"));

                        if (featured_Type.equalsIgnoreCase("Category"))
                            mAppsItemCat.add(categoryitem);

                        if (featured_Type.equalsIgnoreCase("Language"))
                            mAppsItemLang.add(categoryitem);

                        break;
                }
            }

            //notify data changes to to the list adapater
            mRECAppsDisplayAdapter.notifyDataSetChanged();
            mTOPAppsDisplayAdapter.notifyDataSetChanged();

            gridviewCat.setAdapter(new CategoryItemAdapter(getActivity(), mAppsItemCat, "category", fm));
            gridviewLang.setAdapter(new CategoryItemAdapter(getActivity(), mAppsItemLang, "language", fm));

        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
    }


    @Override
    public void onClick(View v)
    {
        String logMessage = null;
        Bundle b = new Bundle();

        switch(v.getId())
        {
            case R.id.btn_featured:
                b.putString("title", getString(R.string.featured_apps));
                b.putString("type", "featured");
                b.putSerializable("data", (Serializable) mAppsItemRecommended);
                break;

            case R.id.btn_top:
                b.putString("title", getString(R.string.top_apps));
                b.putString("type", "top");
                b.putSerializable("data", (Serializable) mAppsItemTops);
                break;

            case R.id.btn_lang:
                b.putString("title", getString(R.string.lang));
                b.putString("type", "lang");
                b.putSerializable("data", (Serializable) mAppsItemLang);
                break;

            case R.id.btn_cat:
                b.putString("title", getString(R.string.categ));
                b.putString("type", "cat");
                b.putSerializable("data", (Serializable) mAppsItemCat);
                break;
        }

        SeeMoreFragment app_frag = new SeeMoreFragment();
        app_frag.setArguments(b);

        String FragTAG = "SEEMORE_FRAG";
        fragmentTransaction = fm.beginTransaction();
        fragmentTransaction
                .setCustomAnimations(R.anim.slide_in_from_top, R.anim.slide_out_to_bottom, R.anim.slide_in_from_top, R.anim.slide_out_to_bottom)
                .replace(R.id.lay_1, app_frag, FragTAG)
                .addToBackStack(FragTAG)
                .commit();
    }


    private BroadcastReceiver UpdateGridListReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent)
        {
            String didAppDownload = intent.getStringExtra("AppDownload");
            if(!didAppDownload.equalsIgnoreCase("False"))
                updateItemAtPosition(didAppDownload);

        }
    };

    private void updateItemAtPosition(String AppStatus)
    {
        AppItem item;
        String STAT = "FREE";
        if(AppStatus.equalsIgnoreCase("True_Install"))
            STAT = "INSTALLED";

        if(RecApp_Position > 0)
        {
            item = (AppItem) mRECAppsDisplayAdapter.getItem(RecApp_Position);
            item.setAppPrice(STAT);
            mRECAppsDisplayAdapter.notifyDataSetChanged();
            gridviewRec.invalidateViews();
            gridviewRec.setAdapter(mRECAppsDisplayAdapter);
        }

        if(TopApp_Position > 0)
        {
            item = (AppItem) mTOPAppsDisplayAdapter.getItem(TopApp_Position);
            item.setAppPrice(STAT);
            mTOPAppsDisplayAdapter.notifyDataSetChanged();
            gridviewTop.invalidateViews();
            gridviewTop.setAdapter(mTOPAppsDisplayAdapter);
        }
    }

    @Override
    public void onDestroy()
    {
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(UpdateGridListReceiver);
        super.onDestroy();
    }

    @Override
    public void onWidgetImageCompleted(List<Bitmap> bitmaps)
    {
        if(bitmaps == null || bitmaps.size() < 1)
            return;

        WidgetViewPager.setAdapter(new ImagePagerAdapter(getActivity(), bitmaps));
        WidgetIndicator.setViewPager(WidgetViewPager);

        WidgetViewPager.setInterval(10000);
        WidgetViewPager.setSlideBorderMode(AutoScrollViewPager.SLIDE_BORDER_MODE_TO_PARENT);

        WidgetViewPager.startAutoScroll();
    }
}
