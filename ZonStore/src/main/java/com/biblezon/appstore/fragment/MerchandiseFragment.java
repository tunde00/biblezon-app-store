package com.biblezon.appstore.fragment;

import android.content.Intent;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.biblezon.appstore.MerchantWebActivity;
import com.biblezon.appstore.R;

/**
 * Created by tunde00 on 11/11/2015.
 */
public class MerchandiseFragment extends Fragment {

    View mView;
    View layIgnaciusPress;
    View layCatholicSupply;
    View layCatholicCompany;
    View layEWTN;

    FragmentManager fm;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        if(mView == null)
        {
            mView = inflater.inflate(R.layout.merchandise_frag, container, false);
            InitializeControls(mView);
        }

        ViewGroup parent = (ViewGroup) mView.getParent();
        if (parent != null) {
            parent.removeView(mView);
        }

        return mView;
    }

    private void InitializeControls(View v)
    {
        layCatholicCompany = v.findViewById(R.id.catholiccompanylayoutview);
        layCatholicSupply = v.findViewById(R.id.catholicsupplylayoutview);
        layIgnaciusPress = v.findViewById(R.id.ignatiouspresslayoutview);
        layEWTN = v.findViewById(R.id.ewtnlayoutview);

        fm = getActivity().getSupportFragmentManager();
    }
}
