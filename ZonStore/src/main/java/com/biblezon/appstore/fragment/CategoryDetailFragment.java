package com.biblezon.appstore.fragment;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.biblezon.appstore.R;
import com.biblezon.appstore.adapter.AppAdapter;
import com.biblezon.appstore.model.AppItem;
import com.biblezon.appstore.model.CategoryItem;
import com.biblezon.appstore.rpc.AppController;
import com.biblezon.appstore.rpc.VolleyErrorHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by tunde00 on 11/12/2015.
 * NOTE THAT THE LANGUAGE AND CATEGORY DETAIL
 * WILL USE THE SEE MORE LAYOUT
 */
public class CategoryDetailFragment extends Fragment {

    private List<AppItem> mAppItemList;
    private CategoryItem mAppCategory;
    private CategoryDetailFragment me;
    private TextView headerTv;

    private GridView gridView;
    private static int Position;
    private AppAdapter mAppsDisplayAdapter;
    private static final String ACTION_CALLBACK_PAGE = "CategoryAppsListViewCallBack";

    View mView;
    FragmentManager fm;
    FragmentTransaction fragmentTransaction;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        if(mView == null)
        {
            mView = inflater.inflate(R.layout.see_more, container, false);
            init(mView);
            getArgument();
            loadJSONData();
        }

        ViewGroup parent = (ViewGroup) mView.getParent();
        if (parent != null) {
            parent.removeView(mView);
        }

        return mView;
    }

    private void init(View v)
    {
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(
                UpdateCategoryListReceiver, new IntentFilter(ACTION_CALLBACK_PAGE));

        mAppItemList = new ArrayList<>();
        fm = getActivity().getSupportFragmentManager();
        gridView = (GridView) v.findViewById(R.id.gridview);
        headerTv = (TextView) v.findViewById(R.id.textViewHeader);

        gridView.setFocusable(true);
        gridView.setFocusableInTouchMode(true);
        gridView.post(new Runnable() {
            @Override
            public void run() {
                gridView.smoothScrollToPosition(0);
            }
        });

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                if (position < 0)
                    return;

                Position = position;
                AppItem item = mAppItemList.get(position);
                item.setRequestedUIPage(ACTION_CALLBACK_PAGE);

                Log.v("pos", "" + position);
                AppDetailFragment app_frag = new AppDetailFragment();
                Bundle b = new Bundle();
                b.putSerializable("data", (Serializable) item);
                app_frag.setArguments(b);

                fragmentTransaction = fm.beginTransaction();
                fragmentTransaction.setCustomAnimations(R.anim.translucent_enter, R.anim.translucent_exit,
                        R.anim.translucent_enter, R.anim.translucent_exit);
                fragmentTransaction.replace(R.id.lay_1, app_frag, null);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });

        mAppsDisplayAdapter = new AppAdapter(getActivity(), mAppItemList);
        gridView.setAdapter(mAppsDisplayAdapter);
    }

    private void getArgument()
    {
        Bundle b = getArguments();
        mAppCategory = (CategoryItem) b.getSerializable("data");
    }

    private void loadJSONData()
    {
        if(mAppCategory == null)
            return;

        if(mAppCategory.getCategoryType().equalsIgnoreCase("language"))
            headerTv.setText(mAppCategory.getCategoryName() + " Apps");
        else
            headerTv.setText(mAppCategory.getCategoryName());

        final String tag_json_obj = "categorydetailfragment_json_req";
        String URL = mAppCategory.getCategoryType().equalsIgnoreCase("Category") ?
                getResources().getString(R.string.app_category_list) :
                getResources().getString(R.string.app_language_list);

        final ProgressDialog pDialog = new ProgressDialog(this.getActivity());
        pDialog.setMessage("loading...");
        pDialog.show();

        HashMap<String, String> _params = new HashMap<String,String>();
        _params.put(mAppCategory.getCategoryType().toLowerCase(), mAppCategory.getCategoryName() + "");
        JSONObject jsonParams = null != _params ? new JSONObject(_params) :	null;

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                URL, jsonParams,
                new Response.Listener<JSONObject>()
                {
                    @Override
                    public void onResponse(JSONObject response)
                    {
                        VolleyLog.d(tag_json_obj, response.toString());
                        pDialog.hide();

                        if(response == null)
                        {
                            new AlertDialog.Builder(getActivity())
                                    .setMessage(Html.fromHtml("Unable to reach App server at this time <br>Please try again later!"))
                                    .show();
                            return;
                        }

                        ProcessServerResponse(response);
                    }
                },
                new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error)
                    {
                        VolleyLog.d(tag_json_obj, "Error: " + error.getMessage());
                        pDialog.hide();

                        new AlertDialog.Builder(getActivity())
                                .setMessage(Html.fromHtml("Oops experiencing network issues:<br>" + VolleyErrorHelper.getMessage(error, getActivity()) ))
                                .show();
                    }
                }
        );

        int socketTimeout = 60000;//60 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        jsonObjReq.setRetryPolicy(policy);

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);

    }

    private void ProcessServerResponse(final JSONObject response)
    {
        try {

            JSONArray appsArray = response.isNull("language") ?
                    response.getJSONArray("category") :
                    response.getJSONArray("language");

            for (int i = 0; i < appsArray.length(); i++)
            {
                JSONObject appObj = (JSONObject) appsArray.get(i);
                AppItem item = new AppItem();

                item.setAppID(appObj.getInt("id"));
                item.setAppName(appObj.getString("name")); //label of the app
                item.setAppDesc(appObj.getString("desc"));
                item.setAppSummaryDesc(appObj.getString("desc"));
                item.setAppRating(appObj.getString("rating"));
                item.setAppPix(appObj.getString("thumb_url"));

                item.setAppSourceFileURL(appObj.getString("source_url"));
                item.setAppPackageName(appObj.getString("packagename"));
                item.setAppVerCode(appObj.getString("versioncode"));
                item.setAppFileName(appObj.getString("filename"));

                if(!appObj.isNull("language"))
                {
                    item.setAppLanguage(appObj.getString("language"));
                }

                if(!appObj.isNull("pricing"))
                {
                    item.setAppPrice( appObj.getString("pricing").equalsIgnoreCase("0") ? "Free" :
                            appObj.getString("pricing") );
                }

                mAppItemList.add(0, item);
            }

            //notify data changes to to the list adapater
            mAppsDisplayAdapter.notifyDataSetChanged();
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
    }


    private BroadcastReceiver UpdateCategoryListReceiver  = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent)
        {
            String didAppDownload = intent.getStringExtra("AppDownload");
            if(!didAppDownload.equalsIgnoreCase("False"))
                updateItemAtPosition(didAppDownload);
        }
    };

    private void updateItemAtPosition(String AppStatus) {
        int visiblePosition = gridView.getFirstVisiblePosition();
        View view = gridView.getChildAt(Position - visiblePosition);

        String STAT = "FREE";
        if (AppStatus.equalsIgnoreCase("True_Install")) STAT = "INST";

        AppItem item = (AppItem) mAppsDisplayAdapter.getItem(Position);
        item.setAppPrice(STAT);
        mAppsDisplayAdapter.notifyDataSetChanged();
        gridView.invalidateViews();
        gridView.setAdapter(mAppsDisplayAdapter);
    }


    @Override
    public void onDestroy()
    {
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(UpdateCategoryListReceiver);
        super.onDestroy();
    }
}
