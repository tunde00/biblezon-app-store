package com.biblezon.appstore.fragment;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.biblezon.appstore.R;
import com.biblezon.appstore.adapter.AppAdapter;
import com.biblezon.appstore.model.AppItem;
import com.biblezon.appstore.rpc.AppController;
import com.biblezon.appstore.rpc.VolleyErrorHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by tunde00 on 12/6/2015.
 */
public class SearchFragment extends Fragment
{
    private View mView;
    private TextView txtCriteria;
    private Button btnSearchBack;
    private GridView gridviewSearch;

    private List<AppItem> mAppsItemSearch;
    private AppAdapter mAppsDisplayAdapter;
    private static final String ACTION_CALLBACK_PAGE = "SearcAppsListViewCallBack";

    SearchFragment me;
    FragmentManager fm;
    FragmentTransaction fragmentTransaction;

    private static int Position;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        if(mView == null)
        {
            mView = inflater.inflate(R.layout.search_frag, container, false);

            InitializeControls(mView);

            Bundle bundle = getArguments();
            String query = bundle.getString("searchQuery");
            FetchSearchResult(query);
        }

        ViewGroup parent = (ViewGroup) mView.getParent();
        if (parent != null) {
            parent.removeView(mView);
        }

        return mView;
    }


    private void InitializeControls(View v)
    {
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(
                UpdateListReceiver, new IntentFilter(ACTION_CALLBACK_PAGE));

        me = this;

        fm = getActivity().getSupportFragmentManager();

        txtCriteria = (TextView) v.findViewById(R.id.triggerSearchTxt);
        btnSearchBack = (Button) v.findViewById(R.id.triggerSearchBackBtn);

        btnSearchBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                me.getFragmentManager().popBackStack();
            }
        });

        gridviewSearch = (GridView) v.findViewById(R.id.gridviewSearch);
        gridviewSearch.setFocusable(true);
        gridviewSearch.setFocusableInTouchMode(true);
        gridviewSearch.post(new Runnable() {
            @Override
            public void run() {
                gridviewSearch.smoothScrollToPosition(0);
            }
        });

        gridviewSearch.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                if (position < 0)
                    return;

                Position = position;
                AppItem item = mAppsItemSearch.get(position);
                item.setRequestedUIPage(ACTION_CALLBACK_PAGE);

                Log.v("pos", "" + position);
                AppDetailFragment app_frag = new AppDetailFragment();
                Bundle b = new Bundle();
                b.putSerializable("data", (Serializable) item);
                app_frag.setArguments(b);

                fragmentTransaction = fm.beginTransaction();
                fragmentTransaction.setCustomAnimations(R.anim.translucent_enter, R.anim.translucent_exit,
                        R.anim.translucent_enter, R.anim.translucent_exit);
                fragmentTransaction.replace(R.id.lay_1, app_frag, null);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });

        mAppsItemSearch = new ArrayList<AppItem>();
        mAppsDisplayAdapter = new AppAdapter(getActivity(), mAppsItemSearch);
        gridviewSearch.setAdapter(mAppsDisplayAdapter);

    }

    private void FetchSearchResult(String searchQuery)
    {
        txtCriteria.setText(Html.fromHtml("<i><b>Search : </b></i>") + searchQuery);

        final String tag_json_obj = "searchview_json_req";
        String URL = getResources().getString(R.string.app_search_list);

        final ProgressDialog pDialog = new ProgressDialog(getActivity());
        pDialog.setMessage("Searching for Apps...");
        pDialog.show();

        HashMap<String, String> _params = new HashMap<String,String>();
        _params.put("searchquery", searchQuery);
        JSONObject jsonParams = new JSONObject(_params);

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                URL, jsonParams,
                new Response.Listener<JSONObject>()
                {
                    @Override
                    public void onResponse(JSONObject response)
                    {
                        VolleyLog.d(tag_json_obj, response.toString());
                        pDialog.hide();

                        if(response == null)
                        {
                            new android.support.v7.app.AlertDialog.Builder(getActivity())
                                    .setMessage(Html.fromHtml("Unable to reach App server at this time <br>Please try again later!"))
                                    .show();
                            return;
                        }

                        ProcessServerResponse(response);
                    }
                },
                new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error)
                    {
                        VolleyLog.d(tag_json_obj, "Error: " + error.getMessage());
                        pDialog.hide();

                        new AlertDialog.Builder(getActivity())
                                .setMessage(Html.fromHtml("Oops experiencing network issues:<br>" + VolleyErrorHelper.getMessage(error, getActivity()) ))
                                .show();
                    }
                }
        );

        int socketTimeout = 60000;//60 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        jsonObjReq.setRetryPolicy(policy);

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
    }

    private void ProcessServerResponse(final JSONObject response)
    {
        try
        {
            JSONArray appsArray = response.getJSONArray("apps");
            for (int i = 0; i < appsArray.length(); i++)
            {
                JSONObject appObj = (JSONObject) appsArray.get(i);
                AppItem item = new AppItem();

                item.setAppID(appObj.getInt("id"));
                item.setAppName(appObj.getString("name")); //label of the app
                item.setAppDesc(appObj.getString("desc"));
                item.setAppSummaryDesc(appObj.getString("summary"));
                item.setAppRating(appObj.getString("rating"));
                item.setAppPix(appObj.getString("thumb_url"));

                item.setAppSourceFileURL(appObj.getString("source_url"));
                item.setAppPackageName(appObj.getString("packagename"));
                item.setAppVerCode(appObj.getString("versioncode"));
                item.setAppFileName(appObj.getString("filename"));

                if(!appObj.isNull("language"))
                {
                    item.setAppLanguage(appObj.getString("language"));
                }

                if(!appObj.isNull("pricing"))
                {
                    item.setAppPrice( appObj.getString("pricing").equalsIgnoreCase("0") ? "Free" :
                            appObj.getString("pricing") );
                }

                mAppsItemSearch.add(0, item);
            }

            //notify data changes to to the list adapater
            mAppsDisplayAdapter.notifyDataSetChanged();

        }
        catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private BroadcastReceiver UpdateListReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent)
        {
            String didAppDownload = intent.getStringExtra("AppDownload");
            if(!didAppDownload.equalsIgnoreCase("False"))
                updateItemAtPosition(didAppDownload);

        }
    };

    private void updateItemAtPosition(String AppStatus) {
        String STAT = "FREE";
        if (AppStatus.equalsIgnoreCase("True_Install")) STAT = "INST";

        AppItem item = (AppItem) mAppsDisplayAdapter.getItem(Position);
        item.setAppPrice(STAT);
        mAppsDisplayAdapter.notifyDataSetChanged();
        gridviewSearch.invalidateViews();
        gridviewSearch.setAdapter(mAppsDisplayAdapter);
    }


    @Override
    public void onDestroy()
    {
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(UpdateListReceiver);
        super.onDestroy();
    }


}
