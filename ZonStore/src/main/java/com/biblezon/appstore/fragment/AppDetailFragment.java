package com.biblezon.appstore.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.biblezon.appstore.R;
import com.biblezon.appstore.model.AppItem;
import com.biblezon.appstore.rpc.AppController;
import com.biblezon.appstore.util.ApkUtil;

/**
 * Created by tunde00 on 11/11/2015.
 */
public class AppDetailFragment extends Fragment
{
    private ImageLoader mImageLoader = AppController.getInstance().getImageLoader();
    private ApkUtil mApkUtility;
    private ProgressBar mProgressBar;
    private Button mDownloadBtn;
    private Button mOpenBtn;
    private Button mCloseBtn;

    private TextView mAppDesc;
    private TextView mAppTitle;
    private NetworkImageView mAppImg;

    AppDetailFragment me;
    private static String appDownloaded;
    private static String appSourcePage;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View mView = inflater.inflate(R.layout.app_detail, container, false);
        init(mView);
        getArgument();

        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(
                mUpdateUIReceiver, new IntentFilter("AppDetailCallBack"));

        me = this;

        return mView;
    }

    private void init(View v)
    {
        mAppTitle = (TextView) v.findViewById(R.id.app_title);
        mAppDesc = (TextView) v.findViewById(R.id.app_description);
        mAppImg = (NetworkImageView) v.findViewById(R.id.app_icon);
        mProgressBar = (ProgressBar) v.findViewById(R.id.download_progress_bar);
        mDownloadBtn = (Button) v.findViewById(R.id.download_button);
        mCloseBtn = (Button) v.findViewById(R.id.close_button);
        mOpenBtn = (Button) v.findViewById(R.id.open_button);

        appDownloaded = "False";
        mApkUtility = new ApkUtil(getActivity());
    }

    private void getArgument()
    {
        try
        {
            final AppItem item = (AppItem) getArguments().get("data");

            mAppImg.setImageUrl(item.getAppPix(), mImageLoader);
            mAppTitle.setText(Html.fromHtml("<h4>" + item.getAppName().trim() + "</h4>"));
            mAppDesc.setText(Html.fromHtml("<font color='white' size='20px'> <b>" + item.getAppDescription() + "</b></font>"));
            appSourcePage = item.getRequestedUIPage();

            if(item.getAppStatus().equalsIgnoreCase("new"))
            {
                mDownloadBtn.setText("INSTALL");
                mOpenBtn.setVisibility(View.GONE);
            }
            if(item.getAppStatus().toLowerCase().startsWith("exist"))
            {
                mDownloadBtn.setText("UNINSTALL");
                if(item.getAppStatus().equalsIgnoreCase("EXISTING SYS APPS"))
                    mAppDesc.setText( mAppDesc.getText().toString()
                            + Html.fromHtml("<br><br><font color=red> <i>  PLEASE NOTE: This is a system app, only updates will be uninstalled!</i></font>"));
            }

            if(item.getAppStatus().equalsIgnoreCase("update"))
                mDownloadBtn.setText("UPDATE");


            mCloseBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v)
                {
                    me.getFragmentManager().popBackStack();
                }
            });

            mDownloadBtn.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    final Button b = ((Button)v);
                    String txt = (String) b.getText();
                    if(txt.startsWith("UNINSTALL"))
                    {
                        mApkUtility.UnInstallApp(item.getAppPackageName());
                        return;
                    }

                    b.setText("Downloading....");
                    mCloseBtn.setEnabled(false);
                    mDownloadBtn.setEnabled(false);

                    mProgressBar.setVisibility(View.VISIBLE);

                    mApkUtility.DownloadAPK(
                            item.getAppSourceFileURL(),
                            item.getAppName(),
                            item.getAppPackageName(), v,
                            item.getAppFileName(),
                            item.getAppLanguage());
                }
            });

            mOpenBtn.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    try {
                        mApkUtility.openApp(item.getAppPackageName());
                    } catch (PackageManager.NameNotFoundException e) {
                        Toast.makeText(v.getContext(), "Unable to open App", Toast.LENGTH_LONG).show();
                    }
                }
            });

        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    private BroadcastReceiver mUpdateUIReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent)
        {
            String actionResult = intent.getStringExtra("Status");
            mProgressBar.setVisibility(View.GONE);

            if(actionResult == "INSTALL_COMPLETE")
            {
                mOpenBtn.setVisibility(View.VISIBLE);
                mDownloadBtn.setText("UNINSTALL");
                mDownloadBtn.setEnabled(true);
                mCloseBtn.setEnabled(true);
                appDownloaded = "True_Install";
            }

            if(actionResult == "UNINSTALL_COMPLETE" || actionResult == "DOWNLOAD_FAILED")
            {
                mOpenBtn.setVisibility(View.GONE);
                mDownloadBtn.setText("INSTALL");
                mDownloadBtn.setEnabled(true);
                mCloseBtn.setEnabled(true);

               if(actionResult == "UNINSTALL_COMPLETE")
                   appDownloaded = "True_UnInstall";
               else {
                   appDownloaded = "False";

                   Toast.makeText(getActivity(),
                           Html.fromHtml("Download Failed temporarily,<br>Please try again later "),
                           Toast.LENGTH_LONG);
               }
            }

        }
    };

    @Override
    public void onDestroy()
    {
        if(appSourcePage != null && appSourcePage.length() > 0)
        {
            Intent intent = new Intent(appSourcePage);
            intent.putExtra("AppDownload",appDownloaded);
            LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
        }

        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(mUpdateUIReceiver);
        super.onDestroy();
    }
}
