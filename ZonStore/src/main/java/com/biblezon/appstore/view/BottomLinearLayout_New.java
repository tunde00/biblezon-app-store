package com.biblezon.appstore.view;

import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.biblezon.appstore.R;

public class BottomLinearLayout_New extends LinearLayout {

	private Handler q;
	private Context a;
	private int p;

	ImageView appImg;
	ImageView merchandiseImg; 
	ImageView accountImg; 

	LinearLayout appLinearLayout;
	LinearLayout merchandiseLinearLayout;
	LinearLayout accountLinearLayout;
	LinearLayout bottom_layout;

	TextView downloadcountTxt; 



	public BottomLinearLayout_New(Context context)
	{
		super(context, null);
		q = new Handler();
		a = context;
		p = 0;


	}

	public BottomLinearLayout_New(Context context, AttributeSet attributeset)
	{
		super(context, attributeset);
		p = 0;
		q = new Handler();
		a = context;

		bottom_layout = (LinearLayout)LayoutInflater.from(a).inflate(R.layout.bottom_layout_new, this);	

		appLinearLayout = (LinearLayout)bottom_layout.findViewById(R.id.ll_bottom_app);
		merchandiseLinearLayout = (LinearLayout)bottom_layout.findViewById(R.id.ll_bottom_merchandise);
		accountLinearLayout = (LinearLayout)bottom_layout.findViewById(R.id.ll_bottom_account);

		appImg = (ImageView)bottom_layout.findViewById(R.id.iv_bottom_app);
		accountImg = (ImageView)bottom_layout.findViewById(R.id.iv_bottom_account);
		merchandiseImg = (ImageView)bottom_layout.findViewById(R.id.iv_bottom_merchandise);


		downloadcountTxt = (TextView)bottom_layout.findViewById(R.id.tv_download_count);


		currentSegment(0);
		q.postDelayed(new Runnable() {
			@Override
			public void run() {                   
				q.postDelayed(this, 1000);
			}
		}, 1500);
	}

	public void setDownloadCount(BottomLinearLayout_New bottomlinearlayout, int counter)
	{
		bottomlinearlayout.downloadcountTxt.setText(String.valueOf(counter));
		TextView textview = bottomlinearlayout.downloadcountTxt;

		int j1 = View.GONE;
		if (counter > 0)
		{
			j1 = View.VISIBLE;
		} else {
			j1 = View.GONE;
		}
		textview.setVisibility(j1);
	}

	public final void currentSegment(int i1)
	{
		switch (i1)
		{
		default: //hightlight the account

			appImg.setImageDrawable(getResources().getDrawable(R.mipmap.app_selected));
			downloadcountTxt.setTextColor(getResources().getColor(R.color.android_black));
			merchandiseImg.setImageDrawable(getResources().getDrawable(R.mipmap.merchandise_non));
			accountImg.setImageDrawable(getResources().getDrawable(R.mipmap.menu_non));

			return;

		case 0:  // highlight the apps

			appImg.setImageDrawable(getResources().getDrawable(R.mipmap.app_selected));
			downloadcountTxt.setTextColor(getResources().getColor(R.color.android_black));
			merchandiseImg.setImageDrawable(getResources().getDrawable(R.mipmap.merchandise_non));
			accountImg.setImageDrawable(getResources().getDrawable(R.mipmap.menu_non));

			return;

		case 1: // highlight the merchandize

			appImg.setImageDrawable(getResources().getDrawable(R.mipmap.app_non));
			downloadcountTxt.setTextColor(getResources().getColor(R.color.android_black));
			merchandiseImg.setImageDrawable(getResources().getDrawable(R.mipmap.merchandise_selected));
			accountImg.setImageDrawable(getResources().getDrawable(R.mipmap.menu_non));
			return;	

		case 2: // highlight the merchandize

			accountImg.setImageDrawable(getResources().getDrawable(R.mipmap.menu_selected));
			downloadcountTxt.setTextColor(getResources().getColor(R.color.android_black));
			merchandiseImg.setImageDrawable(getResources().getDrawable(R.mipmap.merchandise_non));
			appImg.setImageDrawable(getResources().getDrawable(R.mipmap.app_non));
			return;	
		}   

	}  



}
