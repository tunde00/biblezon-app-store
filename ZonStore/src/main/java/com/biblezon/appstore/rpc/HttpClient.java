package com.biblezon.appstore.rpc;

import android.content.Context;

import com.android.volley.Cache;
import com.android.volley.Cache.Entry;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request.Method;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.biblezon.appstore.notifier.EventCallback;
import com.biblezon.appstore.notifier.EventNotifier;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

public class HttpClient {
	
	private static final String TAG = "HttpClient";
	private boolean enableCache = false;
	String  HttpStringResponse = "";
	static HttpClient mInstance;
	
	EventNotifier notifier; //works like c# delegate
	
	public HttpClient(EventCallback evc){
		
		notifier = new EventNotifier(evc);
	}		
	
	public static synchronized HttpClient getInstance(EventCallback evC) {
		if (null == mInstance) {
			mInstance = new HttpClient(evC);
		}
		else{
			mInstance.notifier = null;
			mInstance.notifier = new EventNotifier(evC);
		}
		return mInstance;
	}
	
	public void setCache(boolean enable){ this.enableCache  = enable; }
	
	public void JsonPost(String actionAlias, String httpPage, HashMap<String, String> _params)
	{
		final String action = actionAlias;
		String URL_FEED =  httpPage;
		JSONObject jsonParams = null != _params ? new JSONObject(_params) :	null;
		
		Cache cache = AppController.getInstance().getRequestQueue().getCache();
		Entry entry = cache.get(URL_FEED);
		
		if (entry != null && enableCache)
		{
			// fetch the data from cache
			try {
				String data = new String(entry.data, "UTF-8");
				try {
					parseJsonFeed(action, new JSONObject(data));
				} catch (JSONException e) {
					e.printStackTrace();
				}
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}

		} else {
		
			// making fresh volley request and getting json
			JsonObjectRequest jsonReq = new JsonObjectRequest(Method.POST,
					URL_FEED, jsonParams, new Response.Listener<JSONObject>() {
	
						@Override
						public void onResponse(JSONObject response) {
							VolleyLog.d(TAG, "Response: " + response.toString());
							if (response != null) {
								parseJsonFeed(action, response);
							}
						}
						
					}, new Response.ErrorListener() {
	
						@Override
						public void onErrorResponse(VolleyError error) {
							VolleyLog.d(TAG, "Error: " + error.getMessage());
							parseHttpResp(action, error.getMessage());
						}
					});


			int socketTimeout = 60000;//60 seconds - change to what you want
			RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
			jsonReq.setRetryPolicy(policy);

			// Adding request to volley request queue
			AppController.getInstance().addToRequestQueue(jsonReq);
		}
	}
	
	public void HttpPost(String actionAlias, String httpPage, HashMap<String, String> _params){
				
		final HashMap<String, String> params = _params;
		String URL =  httpPage;
		final String action = actionAlias;
			
		StringRequest myReq = new StringRequest( Method.POST,URL,
			new Response.Listener<String>() {
				@Override
		        public void onResponse(String response) {
					parseHttpResp(action, response);
				}
			},
			new Response.ErrorListener(){				 
				@Override
		         public void onErrorResponse(VolleyError error) {
		             // error
					parseHttpResp(action, error.getMessage());
		       }
			})
		{
			protected Map<String, String> getParams() throws com.android.volley.AuthFailureError {
		         return params;
		    };
		};
		
		AppController.getInstance().addToRequestQueue(myReq);
	}
	
	public void JsonAssetPost(String actionAlias, String assetJsonName, Context context) 
	{
		 String json = null;
		    try {

		        InputStream is = context.getAssets().open(assetJsonName);

		        int size = is.available();

		        byte[] buffer = new byte[size];

		        is.read(buffer);

		        is.close();

		        json = new String(buffer, "UTF-8");


		    } catch (IOException ex) {
		        ex.printStackTrace();		   
		    }
		    		    
		    try {
				parseJsonFeed(actionAlias, new JSONObject(json));
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}		    
		    return;
	}
	
	
	private void parseJsonFeed(String action, JSONObject response) {
		notifier.Signal(action, response);
	}
	
	private void parseHttpResp(String action, String response){		
		
		if(response == null){
			action = "unable_to_connect"; 
		}
		else if(response.contains("(Network is unreachable)") || 
		   response.contains("java.net.ConnectException:") ||
		   response.contains("org.json.JSONException"))
		{
			action = "unable_to_connect"; // this will render the app void
		}
		
		notifier.Signal(action, response);
	}
	
}
