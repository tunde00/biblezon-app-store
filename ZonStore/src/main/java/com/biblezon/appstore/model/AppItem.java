package com.biblezon.appstore.model;

/**
 * Created by tunde00 on 11/11/2015.
 */

import java.io.Serializable;
import android.os.Parcel;
import android.os.Parcelable;

@SuppressWarnings("serial")
public class AppItem  implements Serializable {

    private String m_strAppName;
    private String m_strAppVersionCode;         // i.e. Will determine the version no of the apk indicating if its new version
    private String m_strAppDescription;
    private String m_strAppSummaryDesc;
    private String m_strAppRating;
    private String m_strAppPix;

    private String m_strAppFileName;       // i.e. Apk Name
    private String m_strAppPackageName;    // i.e. Apk Package Name e.g. com.biblezon.appstore
    private String m_strAppSourceFileUrl;  // i.e. the location of the actual apk file e.g. http://biblezonadmin.com/biblezon/zonstore/apk/AppName.apk


    private String m_strAppStatus;  // i.e. Free, Update, Uninstall. Note if Free then new install is hinted
    private String m_strAppPrice;
    private String m_strAppLanguage;

    private String m_RequestedPage;  // page making this request from the ui


    private boolean m_bIsInstalled;
    private int m_nID;

    public AppItem()
    {

    }
    public AppItem(int appID, String appName, String appDesc, String appSummary, String appVerCode, String appPix, String appRating, String appPrice, boolean appInst){

        this.m_nID = appID;
        this.m_strAppName = appName;
        this.m_strAppVersionCode = appVerCode ;
        this.m_strAppDescription = appDesc;
        this.m_strAppPix = appPix;
        this.m_bIsInstalled = appInst;
        this.m_strAppRating = appRating;
        this.m_strAppSummaryDesc = appSummary;
        this.m_strAppPrice = appPrice;
        this.m_strAppLanguage = "English";
    }

    public int getAppID(){ return this.m_nID; }
    public String getAppName(){ return this.m_strAppName; }
    public String getAppVersionCode(){ return this.m_strAppVersionCode; }
    public String getAppDescription(){ return this.m_strAppDescription;}
    public String getAppRating(){ return this.m_strAppRating; }
    public String getAppSummaryDesc(){ return this.m_strAppSummaryDesc;}

    public String getAppPix(){ return this.m_strAppPix; }
    public boolean getAppInstalled(){ return this.m_bIsInstalled; }

    public String getAppFileName(){ return this.m_strAppFileName; }
    public String getAppPackageName() { return this.m_strAppPackageName; }
    public String getAppSourceFileURL() { return this.m_strAppSourceFileUrl; }

    public String getAppStatus() { return this.m_strAppStatus; }
    public String getAppPrice(){ return this.m_strAppPrice; }
    public String getAppLanguage() { return this.m_strAppLanguage; }
    public String getRequestedUIPage(){ return this.m_RequestedPage; }

    public void setAppID(int appID){ this.m_nID = appID; }
    public void setAppName(String appName){ this.m_strAppName = appName; }
    public void setAppVerCode(String appVer){ this.m_strAppVersionCode = appVer; }
    public void setAppDesc(String appDesc){ this.m_strAppDescription = appDesc; }
    public void setAppRating(String appRating){ this.m_strAppRating = appRating; }
    public void setAppSummaryDesc(String appSummary){ this.m_strAppSummaryDesc = appSummary; }

    public void setAppPix(String appPix){ this.m_strAppPix = appPix; }
    public void setAppInstalled(boolean appInst){ this.m_bIsInstalled = appInst; }
    public void setAppLanguage(String applang) { this.m_strAppLanguage = applang; }
    public void setRequestedUIPage(String RequestedUIPage){ this.m_RequestedPage = RequestedUIPage; }

    public void setAppFileName(String AppFileName){  this.m_strAppFileName = AppFileName; }
    public void setAppPackageName(String AppPackageName) {  this.m_strAppPackageName = AppPackageName; }
    public void setAppSourceFileURL(String AppSourceFileURL) {  this.m_strAppSourceFileUrl = AppSourceFileURL; }

    public void setAppStatus(String AppStatus){ this.m_strAppStatus = AppStatus; }
    public void setAppPrice(String AppPrice) { this.m_strAppPrice = AppPrice; }

}