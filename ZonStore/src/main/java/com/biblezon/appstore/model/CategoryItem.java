package com.biblezon.appstore.model;

/**
 * Created by tunde00 on 11/11/2015.
 */

import java.io.Serializable;

@SuppressWarnings("serial")
public class CategoryItem implements Serializable {

    private int m_nID;
    private String m_strCategoryName;
    private String m_strCategoryDesc;
    private String m_strCategoryPixUrl;
    private String m_StrCategoryDispName;
    private String m_strCategoryType; //either of App Language or App Category

    public CategoryItem(){

    }

    public int getCategoryID(){ return this.m_nID; }
    public String getCategoryName(){ return this.m_strCategoryName; }
    public String getCategoryDispName() { return this.m_StrCategoryDispName; }
    public String getCategoryDescription(){ return this.m_strCategoryDesc;}
    public String getCategoryPictureUrl(){ return this.m_strCategoryPixUrl; }
    public String getCategoryType(){ return this.m_strCategoryType; }

    public void setCategoryID(int categoryID){ this.m_nID = categoryID; }
    public void setCategoryName(String categoryName){ this.m_strCategoryName = categoryName; }
    public void setCategoryType(String categoryType){ this.m_strCategoryType = categoryType; }
    public void setCategoryDescription(String categoryDesc){ this.m_strCategoryDesc = categoryDesc; }
    public void setCategoryPictureUrl(String categoryPixURL){ this.m_strCategoryPixUrl = categoryPixURL; }
    public void setCategoryDisplayName(String categoryDispName) { this.m_StrCategoryDispName = categoryDispName; }

}

