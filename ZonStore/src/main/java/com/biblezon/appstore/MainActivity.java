package com.biblezon.appstore;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.StrictMode;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.biblezon.appstore.autoupdate.AutoUpdate;
import com.biblezon.appstore.fragment.CategoryDetailFragment;
import com.biblezon.appstore.fragment.MainFragment;
import com.biblezon.appstore.fragment.MerchandiseFragment;
import com.biblezon.appstore.fragment.SearchFragment;
import com.biblezon.appstore.model.CategoryItem;
import com.biblezon.appstore.view.BottomLinearLayout_New;

import java.io.Serializable;

public class MainActivity extends AppCompatActivity implements android.view.View.OnClickListener {

    MainActivity _me;

    View _leftdrawer;
    DrawerLayout _drawerlayout;
    ActionBarDrawerToggle _drawerToggle;

    FragmentManager fm;
    MainFragment app_frag;
    FragmentTransaction fragmentTransaction;
    static LinearLayout frg_layout;

    TextView _downloadTxt;
    BottomLinearLayout_New _bottomLayout;

    Toolbar search_bar;
    EditText search_title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        _me = this;
        frg_layout = (LinearLayout) findViewById(R.id.lay_1);
        initializeDrawerMenu(); //left sliding side menu
        initializeSearchBar();
        initializeBottomMenu(); //bottom menu with segments of the application (Apps, Merchandize, Account)

        if (Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                    .permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        // AUTOMATIC CODE UPDATE *******************************
        AutoUpdate.getInstance(this, this.getResources().getString(R.string.update_app_url),
                this.getResources().getString(R.string.process_broadcast_receiver_response),
                "BibleZon Store", "ZonStore.apk").performCheckforUpgrade();
    }


    /////////////////////////////// PRIVATE METHODS ////////////////////////////////////////////


    @SuppressWarnings("deprecation")
    private void initializeDrawerMenu()
    {
        _leftdrawer = findViewById(R.id.left_drawer);
        _drawerlayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        _drawerToggle = new ActionBarDrawerToggle(
                this,                  /* host Activity */
                _drawerlayout,         /* DrawerLayout object */
                R.drawable.ic_drawer,  /* nav drawer icon to replace 'Up' caret */
                0,  /* R.string.drawer_open"open drawer" description */
                0  /* R.string.drawer_close"close drawer" description */
        )
        {

            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view)
            {
                super.onDrawerClosed(view);
                _drawerlayout.closeDrawer(_leftdrawer);
                //getActionBar().setTitle(R.string.app_name);
            }

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView)
            {
                super.onDrawerOpened(drawerView);
                _drawerlayout.openDrawer(_leftdrawer);
            }

        };

        // Set the drawer toggle as the DrawerListener
        _drawerlayout.setDrawerListener(_drawerToggle);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowHomeEnabled(false);
        actionBar.setBackgroundDrawable(getResources().getDrawable(R.mipmap.title_bar));
        actionBar.setIcon(new ColorDrawable(getResources().getColor(android.R.color.transparent)));
        actionBar.setTitle("");


    }

    private void initializeSearchBar()
    {
        search_bar = (Toolbar)findViewById(R.id.my_awesome_toolbar);
        search_bar.setVisibility(View.GONE);

        search_title = (EditText)findViewById(R.id.searchtitle);
        search_title.addTextChangedListener(new TextWatcher() {

            private String s;
            private long after;
            private Thread t;
            private Runnable runnable_EditTextWatcher = new Runnable() {
                @Override
                public void run() {
                    while (true) {
                        if ((System.currentTimeMillis() - after) > 1000) {
                            Log.d("Debug_EditTEXT_watcher", "(System.currentTimeMillis()-after)>600 ->  " + (System.currentTimeMillis() - after) + " > " + s);

                            Bundle b = new Bundle();
                            b.putString("searchQuery", s);
                            SearchFragment search_frag = new SearchFragment();
                            search_frag.setArguments(b);

                            fragmentTransaction = fm.beginTransaction();
                            fragmentTransaction.setCustomAnimations(R.anim.slide_in_from_top, R.anim.slide_out_to_bottom,
                                    R.anim.slide_in_from_top, R.anim.slide_out_to_bottom);
                            fragmentTransaction.replace(R.id.lay_1, search_frag, null);
                            fragmentTransaction.addToBackStack(null);
                            fragmentTransaction.commit();


                            t = null;
                            break;
                        }
                    }
                }
            };

            @Override
            public void onTextChanged(CharSequence ss, int start, int before, int count) {
                s = ss.toString();
            }


            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable ss) {
                after = System.currentTimeMillis();
                if (t == null) {
                    t = new Thread(runnable_EditTextWatcher);
                    t.start();
                }
            }
        });
        search_title.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    hideKeyboard(v);
                }
            }
        });
    }

    private void initializeBottomMenu()
    {
        _bottomLayout = (BottomLinearLayout_New)findViewById(R.id.ll_bottom_layout);
        _bottomLayout.findViewById(R.id.ll_bottom_app).setOnClickListener(this);
        _bottomLayout.findViewById(R.id.ll_bottom_merchandise).setOnClickListener(this);
        _bottomLayout.findViewById(R.id.ll_bottom_account).setOnClickListener(this);
        _downloadTxt = (TextView)_bottomLayout.findViewById(R.id.tv_download_count);

        _bottomLayout.currentSegment(0);

        app_frag = new MainFragment();
        fm = getSupportFragmentManager();
        fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.replace(R.id.lay_1, app_frag, null);
        fragmentTransaction.commit();
    }

    public void hideKeyboard(View view) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
    }

    /////////////////////////////// EVENTS ////////////////////////////////////////////////////


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.search_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        // Pass the event to ActionBarDrawerToggle, if it returns
        // true, then it has handled the app icon touch event
        if (_drawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if(id == R.id.action_search)
		{
            if(search_bar.getVisibility() == View.VISIBLE)
            {
                search_bar.setVisibility(View.GONE);

                return true;
            }

            search_bar.setVisibility(View.VISIBLE);
            int noOfChild = search_bar.getChildCount();
            View view;

            search_bar.animate()
                    .translationY(0)
                    .alpha(1).setDuration(100)
                    .setInterpolator(new DecelerateInterpolator());

            /* For loop animates toolbar's child elements to give a nice parallax effect */
            for(int i = 0; i < noOfChild; i++ ){
                view = search_bar.getChildAt(i);
                view.setTranslationY(-300);
                view.animate().setStartDelay(500).setDuration(1000).translationY(0);
            }

			return true;
		}
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        // Sync the toggle state after onRestoreInstanceState has occurred.
        _drawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        _drawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId())
        {
            default:
                String TEst = "hello";
                return;

            case R.id.ll_bottom_app:

                _bottomLayout.currentSegment(0);
                String FragTAG = "MainFragment_TAG";
                MainFragment app_frag = new MainFragment();
                fragmentTransaction = fm.beginTransaction();
                fragmentTransaction
                        .replace(R.id.lay_1, app_frag, FragTAG)
                        .addToBackStack(FragTAG)
                        .commit();
                break;

            case R.id.ll_bottom_merchandise:

                _bottomLayout.currentSegment(1);
                String MechantTAG = "Merchandise_TAG";
                MerchandiseFragment m_frag = new MerchandiseFragment();
                fragmentTransaction = fm.beginTransaction();
                fragmentTransaction
                        .replace(R.id.lay_1, m_frag, MechantTAG)
                        .addToBackStack(MechantTAG)
                        .commit();
                break;

            case R.id.ll_bottom_account:
                _drawerlayout.openDrawer(_leftdrawer);
                _bottomLayout.currentSegment(2);
                break;


            case R.id.catholicsupplylayoutview:
            case R.id.catholiccompanylayoutview:
            case R.id.ignatiouspresslayoutview:
            case R.id.ewtnlayoutview:

                String title = "";
                String url = "";

                if(v.getId() == R.id.catholicsupplylayoutview ) {
                    title = "Catholic Supply";
                    url = "http://catholicsupply.americommerce.com";
                }
                if(v.getId() == R.id.catholiccompanylayoutview ) {
                    title = "Catholic Company";
                    url = "http://www.catholiccompany.com/";
                }

                if(v.getId() ==  R.id.ignatiouspresslayoutview) {
                    title = "Ignatious Press";
                    url = "http://www.ignatius.com/";
                }
                if(v.getId() == R.id.ewtnlayoutview) {
                    title = "EWTN";
                    url = "http://www.ewtnreligiouscatalogue.com/";
                }

                if(title == null && url == null)
                    return;

                Intent i = new Intent(this.getApplicationContext(), Browser.class);
                String[] value = new String[]{ title, url };
                i.putExtra("SelectedSite", value);
                startActivity(i);

                break;
        }
    }


    public void onLeftDrawerClick(View v)
    {
        CategoryItem item = null;

        switch(v.getId())
        {
            case R.id.account:
                Intent intent1 = new Intent(this, AccountActivity.class);
                startActivity(intent1);
                break;

            case R.id.btn_media:
                item = new CategoryItem();
                item.setCategoryName( getResources().getString(R.string.media_cat));
                item.setCategoryType("Category");
                FetchAppCategoryFromDrawerMenuAction(item);
                break;

            case R.id.btn_hymns:
                item = new CategoryItem();
                item.setCategoryName( getResources().getString(R.string.hymn_cat));
                item.setCategoryType("Category");
                FetchAppCategoryFromDrawerMenuAction(item);
                break;

            case R.id.btn_reading:
                item = new CategoryItem();
                item.setCategoryName( getResources().getString(R.string.dailyreading_cat));
                item.setCategoryType("Category");
                FetchAppCategoryFromDrawerMenuAction(item);
                break;

            case R.id.btn_prayer:
                item = new CategoryItem();
                item.setCategoryName( getResources().getString(R.string.prayer_cat));
                item.setCategoryType("Category");
                FetchAppCategoryFromDrawerMenuAction(item);
                break;

            case R.id.btn_bible:
                item = new CategoryItem();
                item.setCategoryName( getResources().getString(R.string.bible_cat));
                item.setCategoryType("Category");
                FetchAppCategoryFromDrawerMenuAction(item);
                break;

            case R.id.btn_books:
                item = new CategoryItem();
                item.setCategoryName( getResources().getString(R.string.book_cat));
                item.setCategoryType("Category");
                FetchAppCategoryFromDrawerMenuAction(item);
                break;

            case R.id.btn_kids:
                item = new CategoryItem();
                item.setCategoryName( getResources().getString(R.string.kids_cat));
                item.setCategoryType("Category");
                FetchAppCategoryFromDrawerMenuAction(item);
                break;

            case R.id.btn_apps:
                item = new CategoryItem();
                item.setCategoryName( getResources().getString(R.string.biblezon_cat));
                item.setCategoryType("Category");
                FetchAppCategoryFromDrawerMenuAction(item);
                break;

            case R.id.btn_english:
                item = new CategoryItem();
                item.setCategoryName( getResources().getString(R.string.english_lang));
                item.setCategoryType("Language");
                FetchAppCategoryFromDrawerMenuAction(item);
                break;

            case R.id.btn_spanish:
                item = new CategoryItem();
                item.setCategoryName( getResources().getString(R.string.spanish_lang));
                item.setCategoryType("Language");
                FetchAppCategoryFromDrawerMenuAction(item);
                break;

        }

        _drawerlayout.closeDrawer(_leftdrawer);
    }

    private void FetchAppCategoryFromDrawerMenuAction(CategoryItem item)
    {
        CategoryDetailFragment app_frag = new CategoryDetailFragment();
        Bundle b = new Bundle();
        b.putSerializable("data", (Serializable) item);
        app_frag.setArguments(b);

        String FragTAG = "APPCATEGORYMENU_FRAG";
        fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.setCustomAnimations(
                R.anim.translucent_enter,
                R.anim.translucent_exit,
                R.anim.translucent_enter,
                R.anim.translucent_exit);
        fragmentTransaction.replace(R.id.lay_1, app_frag, FragTAG);
        fragmentTransaction.addToBackStack(FragTAG);
        fragmentTransaction.commit();
    }

//    @Override
//    public void onBackPressed()
//    {
//        super.onBackPressed();
//        popBackStack();
//    }
//
//    public void popBackStack() {
//
//        // Call current fragment's onPopBackStack if it has one.
//        if(getFragmentManager().getBackStackEntryCount()  == 0)
//        {
//            getFragmentManager().popBackStack();
//            return;
//        }
//
//        String fragmentTag = getFragmentManager().getBackStackEntryAt(getFragmentManager().getBackStackEntryCount() - 1).getName();
//        Fragment currentFragment = getSupportFragmentManager().findFragmentByTag(fragmentTag);
//
//        if (currentFragment instanceof BackPressedFragment)
//            ((BackPressedFragment)currentFragment).onPopBackStack();
//        else
//            fm.popBackStack();
//    }
}
