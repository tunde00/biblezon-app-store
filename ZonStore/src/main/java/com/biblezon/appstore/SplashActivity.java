package com.biblezon.appstore;

import android.app.NotificationManager;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ProgressBar;

import com.biblezon.appstore.notifier.EventCallback;
import com.biblezon.appstore.rpc.HttpClient;
import com.biblezon.appstore.util.ApkUtil;
import com.biblezon.appstore.util.DeviceManager;
import com.biblezon.appstore.MainActivity;



/**
 * Created by tunde00 on 10/14/2015.
 */
public class SplashActivity  extends AppCompatActivity implements View.OnClickListener
{
    // Splash screen timer
    private static int SPLASH_TIME_OUT = 1000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        ActionBar actionBar = getSupportActionBar();
        actionBar.hide();

        new Handler().postDelayed(new Runnable() {

            /*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             */

            @Override
            public void run() {
                // This method will be executed once the timer is over
                // Start your app main activity
                Intent i = new Intent(SplashActivity.this, MainActivity.class);
                startActivity(i);

                // close this activity
                finish();
            }
        }, SPLASH_TIME_OUT);
    }

    @Override
    public void onClick(View v) {

    }
}
